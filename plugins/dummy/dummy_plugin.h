#ifndef OMV_DUMMYPLUGIN_H
#define OMV_DUMMYPLUGIN_H

#include "omvnn/base_plugin.h"
#include "omvnn/base_service.h"
#include "omvnn/endpoint.h"

#include "dummy_service.h"

namespace omv {

class DummyPlugin : public IPlugin {
 public:
  DummyPlugin();
  ~DummyPlugin() override = default;

  void ServiceFromProto(const gproto::Any*) override;
  void ServiceToProto(gproto::Any*) const override;
  void Init(BoostAsio*, const Endpoint::Options&, const Endpoint::Options&) override;
  void Start() override;
  void Stop() override;
  bool IsStopped() const override;

 private:
  DummyService::Ptr service_;
};

extern "C" DummyPlugin* Create();
extern "C" void Destroy(DummyPlugin*);
extern "C" void GetDesc(IPlugin::Desc*);

}  // namespace omv

#endif  // OMV_DUMMYPLUGIN_H
