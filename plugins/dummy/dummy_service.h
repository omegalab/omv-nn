#ifndef OMV_DUMMYSERVICE_H
#define OMV_DUMMYSERVICE_H

#include "omv/proto/dummy.pb.h"
#include "omvnn/base_service.h"

namespace omv {

class DummyService : public IService<proto::Dummy> {
 public:
  using Ptr = std::shared_ptr<DummyService>;

  DummyService();
  ~DummyService() override = default;

  void FromProto(const proto::Dummy*) override {}
  void ToProto(proto::Dummy*) const override {}
  void Init(BoostAsio*) override {}
  void Start() override;
  void Stop() override;
  void Process(VideoFrame::Ptr frame) override;
};

}  // namespace omv

#endif  // OMV_DUMMYSERVICE_H
