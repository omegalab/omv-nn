#include "dummy_plugin.h"

namespace omv {

DummyPlugin::DummyPlugin() { service_ = std::make_shared<DummyService>(); }

void DummyPlugin::ServiceFromProto(const google::protobuf::Any* service_proto) {
  proto::Dummy dummy_proto;
  service_proto->UnpackTo(&dummy_proto);
  service_->FromProto(&dummy_proto);
}

void DummyPlugin::ServiceToProto(google::protobuf::Any* service_proto) const {
  proto::Dummy dummy_proto;
  service_->ToProto(&dummy_proto);
  service_proto->PackFrom(dummy_proto);
}

void DummyPlugin::Init(IPlugin::BoostAsio* io, const Endpoint::Options& ctrl_opts,
                       const Endpoint::Options& data_opts) {
  service_->Init(io);

  SetQueueCallback([&](VideoFrame::Ptr frame) {
    //    service_->Process(frame);
    service_->Publish(frame);
  });
  RegisterService(service_);
  InitEndpoints(io, ctrl_opts, data_opts);
}

void DummyPlugin::Start() {
  service_->Start();
  StartQueue(8);
  StartEndpoints();
}

void DummyPlugin::Stop() {
  StopEndpoints();
  StopQueue();
  service_->Stop();
}

bool DummyPlugin::IsStopped() const { return service_->IsStopped(); }

DummyPlugin* Create() { return new DummyPlugin; }
void Destroy(DummyPlugin* plugin) { delete plugin; }
void GetDesc(IPlugin::Desc* desc) {
  desc->type = IPlugin::Type::DUMMY;
  desc->name = "dummy";
  desc->desc = "dummy desc";
  desc->has_queue = true;
}

}  // namespace omv
