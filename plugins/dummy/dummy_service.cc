#include "dummy_service.h"

#include "utils/logger.h"

namespace omv {

DummyService::DummyService() {
  ResetStopEvent();
  SetStopEvent();
}

void DummyService::Start() { LOG(INFO) << "started dummy plugin"; }
void DummyService::Stop() { LOG(INFO) << "stopped dummy plugin"; }

void DummyService::Process(VideoFrame::Ptr frame) { LOG(DEBUG) << "dummy processing"; }

}  // namespace omv
