#include "yolo_detector_service.h"

#include "omvnn/color.h"
#include "utils/options.h"
#include "utils/profile.h"

namespace omv {

YoloDetectorService::YoloDetectorService() {
  ResetStopEvent();
  SetStopEvent();
}

void YoloDetectorService::FromProto(const proto::YoloDetector* proto) {
  if (!proto->data_path().empty())
    data_path_ = proto->data_path();
  if (proto->confidence() > 0)
    confidence_ = proto->confidence();
  if (proto->nms_thresh() > 0)
    nms_threshold_ = proto->nms_thresh();
  if (proto->num_classes())
    num_classes_ = proto->num_classes();
  if (proto->backend())
    backend_ = static_cast<cv::dnn::Backend>(proto->backend());
  if (proto->target())
    target_ = static_cast<cv::dnn::Target>(proto->target());
  if (proto->width())
    width_ = proto->width();
  if (proto->height())
    height_ = proto->height();
}

void YoloDetectorService::ToProto(proto::YoloDetector* proto) const {
  proto->set_data_path(data_path_);
  proto->set_confidence(confidence_);
  proto->set_nms_thresh(nms_threshold_);
  proto->set_num_classes(num_classes_);
  proto->set_backend(static_cast<proto::DnnBackend::Backend>(backend_));
  proto->set_target(static_cast<proto::DnnBackend::Target>(target_));
  proto->set_width(width_);
  proto->set_height(height_);
}

void YoloDetectorService::Init(BoostAsio* io) {
  io_ = io;

  auto& options = Options::Instance();
  const std::string& path_to_data = options["data"];

  fs::path data_path = path_to_data;
  data_path /= data_path_;
  fs::path cfg_path, weigths_path;

  for (const auto& entry : fs::directory_iterator(data_path)) {
    if (entry.is_regular_file()) {
      if (entry.path().extension() == ".cfg") {
        cfg_path = entry.path();
      }
      if (entry.path().extension() == ".weights") {
        weigths_path = entry.path();
      }
      if (entry.path().extension() == ".txt") {
        LoadClassNames(entry.path());
      }
    }
  }

  if (cfg_path.empty() || weigths_path.empty()) {
    throw(std::runtime_error(fmt::format("Yolo failed opening model data {}", data_path.string())));
  }

  net_ = cv::dnn::readNetFromDarknet(cfg_path, weigths_path);
  net_.setPreferableBackend(backend_);
  net_.setPreferableTarget(target_);
  output_names_ = net_.getUnconnectedOutLayersNames();

  LOG(INFO) << "Yolo loaded model";
}

void YoloDetectorService::Process(VideoFrame::Ptr frame) {
  if (IsStopped())
    return;

  cv::Mat blob;
  cv::dnn::blobFromImage(frame->GetMat(), blob, 0.00392, cv::Size{416, 416}, cv::Scalar(), true, false, CV_32F);

  std::vector<cv::Mat> detections;

  {
    std::lock_guard<std::mutex> locker(yd_lock_);
    net_.setInput(blob);
    net_.forward(detections, output_names_);
  }

  std::vector<cv::Rect> boxes;
  std::vector<int> class_id;
  std::vector<float> scores;

  for (auto& output : detections) {
    const auto num_boxes = output.rows;
    for (int i = 0; i < num_boxes; i++) {
      auto itr = std::max_element(output.ptr<float>(i, 5), output.ptr<float>(i, 5 + num_classes_));
      auto confidence = *itr;
      auto classid = static_cast<int>(itr - output.ptr<float>(i, 5));
      if (confidence >= confidence_) {
        auto x = static_cast<int>(output.at<float>(i, 0) * frame->GetMat().cols);
        auto y = static_cast<int>(output.at<float>(i, 1) * frame->GetMat().rows);
        auto width = static_cast<int>(output.at<float>(i, 2) * frame->GetMat().cols);
        auto height = static_cast<int>(output.at<float>(i, 3) * frame->GetMat().rows);
        cv::Rect rect(x - width / 2, y - height / 2, width, height);

        boxes.push_back(rect);
        class_id.push_back(classid);
        scores.push_back(confidence);
      }
    }
  }

  std::vector<int> indices;
  cv::dnn::NMSBoxes(boxes, scores, 0.0, nms_threshold_, indices);
  const cv::Scalar& color = Color::Yellow;

  for (size_t i = 0; i < indices.size(); ++i) {
    auto idx = indices[i];
    const auto& rect = boxes[idx];
    cv::rectangle(frame->GetMat(), cv::Point(rect.x, rect.y), cv::Point(rect.x + rect.width, rect.y + rect.height),
                  color, 3);

    std::ostringstream label_ss;
    label_ss << class_names_[class_id[idx]] << ": " << std::fixed << std::setprecision(2) << scores[idx];
    auto label = label_ss.str();

    int baseline;
    auto label_bg_sz = cv::getTextSize(label.c_str(), cv::FONT_HERSHEY_SIMPLEX, 0.7, 1, &baseline);
    cv::rectangle(frame->GetMat(), cv::Point(rect.x, rect.y - label_bg_sz.height - baseline - 10),
                  cv::Point(rect.x + label_bg_sz.width, rect.y), color, cv::FILLED);
    cv::putText(frame->GetMat(), label.c_str(), cv::Point(rect.x, rect.y - baseline - 2), cv::FONT_HERSHEY_SIMPLEX,
                0.7, cv::Scalar(0, 0, 0));
  }
}
void YoloDetectorService::Start() {
  ResetStopEvent();
  LOG(INFO) << "started yolo detector";
}
void YoloDetectorService::Stop() {
  std::lock_guard<std::mutex> locker(yd_lock_);

  if (!IsStopped()) {
    SetStopEvent();
    LOG(INFO) << "stopped yolo detector";
  }
}

void YoloDetectorService::GetYoloDetectorDnnBackendName(
    ::google::protobuf::RpcController* controller, const ::omv::proto::GetYoloDetectorDnnBackendNameReq* request,
    ::omv::proto::GetYoloDetectorDnnBackendNameRes* response, ::google::protobuf::Closure* done) {
  try {
    /*
    if (nullptr == yd_)
      throw(omv::Exception(fmt::format("no such yolo detector")));

    const auto& backend = yd_->GetBackendName();

    response->set_backend_name(backend);
    */
    throw std::runtime_error("not implemented");
  } catch (const std::exception& e) {
    ErrorResponse(response, e);
  }
}

void YoloDetectorService::GetYoloDetectorDnnTargetName(
    ::google::protobuf::RpcController* controller, const ::omv::proto::GetYoloDetectorDnnTargetNameReq* request,
    ::omv::proto::GetYoloDetectorDnnTargetNameRes* response, ::google::protobuf::Closure* done) {
  try {
    /*
    if (nullptr == yd_)
      throw(omv::Exception(fmt::format("no such yolo detector")));

    const auto& target = yd_->GetTargetName();

    response->set_target_name(target);
    */
    throw std::runtime_error("not implemented");
  } catch (const std::exception& e) {
    ErrorResponse(response, e);
  }
}

void YoloDetectorService::LoadClassNames(const fs::path& path) {
  std::ifstream class_file(path);

  if (!class_file.is_open())
    throw(omv::Exception("Yolo failed opening class names"));

  std::string class_name;
  while (getline(class_file, class_name))
    class_names_.push_back(class_name);
}

}  // namespace omv
