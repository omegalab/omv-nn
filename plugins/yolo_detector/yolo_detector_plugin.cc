#include "yolo_detector_plugin.h"

namespace omv {

YoloDetectorPlugin::YoloDetectorPlugin() { service_ = std::make_shared<YoloDetectorService>(); }

void YoloDetectorPlugin::ServiceFromProto(const gproto::Any* service_proto) {
  proto::YoloDetector yd_proto;
  service_proto->UnpackTo(&yd_proto);
  service_->FromProto(&yd_proto);
}

void YoloDetectorPlugin::ServiceToProto(gproto::Any* service_proto) const {
  proto::YoloDetector yd_proto;
  service_->ToProto(&yd_proto);
  service_proto->PackFrom(yd_proto);
}

void YoloDetectorPlugin::Init(BoostAsio* io, const Endpoint::Options& ctrl_opts,
                              const Endpoint::Options& data_opts) {
  service_->Init(io);

  SetQueueCallback([&](VideoFrame::Ptr frame) {
    service_->Process(frame);
    service_->Publish(frame);
  });

  RegisterService(service_);
  InitEndpoints(io, ctrl_opts, data_opts);
}

void YoloDetectorPlugin::Start() {
  service_->Start();
  StartQueue();
  StartEndpoints();
}

void YoloDetectorPlugin::Stop() {
  StopEndpoints();
  StopQueue();
  service_->Stop();
}

bool YoloDetectorPlugin::IsStopped() const { return service_->IsStopped(); }

YoloDetectorPlugin* Create() { return new YoloDetectorPlugin; }
void Destroy(YoloDetectorPlugin* plugin) { delete plugin; }
void GetDesc(IPlugin::Desc* desc) {
  desc->type = IPlugin::Type::YOLO_DETECTOR;
  desc->name = "yolo detector";
  desc->desc = "yolo detector desc";
}

}  // namespace omv
