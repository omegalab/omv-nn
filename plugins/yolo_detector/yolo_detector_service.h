#ifndef OMV_YOLO_DETECTOR_SERVICE_H
#define OMV_YOLO_DETECTOR_SERVICE_H

#include <filesystem>

#include <boost/asio.hpp>
#include <google/protobuf/arena.h>
#include <opencv2/dnn.hpp>
#include <opencv2/opencv.hpp>

#include "fmt/format.h"
#include "omv/proto/service.pb.h"
#include "omv/proto/video_frame.pb.h"
#include "omv/proto/yolo_detector.pb.h"
#include "omvnn/base_service.h"
#include "utils/helper.h"
#include "utils/logger.h"

namespace gproto = google::protobuf;
namespace asio = boost::asio;
namespace fs = std::filesystem;

namespace omv {

class YoloDetectorService
    : public IService<proto::YoloDetector>
    , public proto::YoloDetectorService {
 public:
  using Ptr = std::shared_ptr<YoloDetectorService>;
  YoloDetectorService();

  void Start() override;
  void Stop() override;
  void FromProto(const proto::YoloDetector*) override;
  void ToProto(proto::YoloDetector*) const override;
  void Init(BoostAsio*) override;
  void Process(VideoFrame::Ptr frame) override;

  // google RPC service stuff
  void GetYoloDetectorDnnBackendName(::google::protobuf::RpcController* controller,
                                     const ::omv::proto::GetYoloDetectorDnnBackendNameReq* request,
                                     ::omv::proto::GetYoloDetectorDnnBackendNameRes* response,
                                     ::google::protobuf::Closure* done) override;

  void GetYoloDetectorDnnTargetName(::google::protobuf::RpcController* controller,
                                    const ::omv::proto::GetYoloDetectorDnnTargetNameReq*,
                                    ::omv::proto::GetYoloDetectorDnnTargetNameRes*,
                                    ::google::protobuf::Closure* done) override;

 private:
  BoostAsio* io_;

  // recieve and process data
  std::mutex yd_lock_;

  // proto and YOLO stuff
  std::string data_path_;
  int32_t num_classes_;
  float confidence_;
  float nms_threshold_;
  int32_t width_;
  int32_t height_;

  // Pure Yolo Stuff
  cv::dnn::Net net_;
  cv::dnn::Backend backend_;
  cv::dnn::Target target_;
  std::vector<std::string> output_names_;
  std::vector<std::string> class_names_;

  void LoadClassNames(const fs::path&);
};

}  // namespace omv

#endif  // OMV_YOLO_DETECTOR_SERVICE_H
