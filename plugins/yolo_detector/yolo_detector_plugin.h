#ifndef OMV_YOLO_DETECTOR_PLUGIN_H
#define OMV_YOLO_DETECTOR_PLUGIN_H

#include "fmt/format.h"
#include "omvnn/base_plugin.h"
#include "omvnn/base_service.h"
#include "omvnn/endpoint.h"

#include "yolo_detector_service.h"

namespace omv {

class YoloDetectorPlugin : public IPlugin {
 public:
  YoloDetectorPlugin();
  ~YoloDetectorPlugin() override = default;

  void ServiceFromProto(const gproto::Any*) override;
  void ServiceToProto(gproto::Any*) const override;
  void Init(BoostAsio*, const Endpoint::Options& ctrl_opts, const Endpoint::Options& data_opts) override;
  void Start() override;
  void Stop() override;
  bool IsStopped() const override;

 private:
  YoloDetectorService::Ptr service_;
};

extern "C" YoloDetectorPlugin* Create();
extern "C" void Destroy(YoloDetectorPlugin*);
extern "C" void GetDesc(IPlugin::Desc*);

}  // namespace omv

#endif  // OMV_YOLO_DETECTOR_PLUGIN_H
