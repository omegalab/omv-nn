#ifndef OMV_VIDEOWRITERPLUGIN_H
#define OMV_VIDEOWRITERPLUGIN_H

#include "boost/signals2.hpp"
#include "omvnn/base_plugin.h"
#include "omvnn/base_service.h"

#include "video_writer_service.h"

namespace omv {

class VideoWriterPlugin : public IPlugin {
 public:
  VideoWriterPlugin();
  ~VideoWriterPlugin() override = default;

  void ServiceFromProto(const gproto::Any*) override;
  void ServiceToProto(gproto::Any*) const override;
  void Init(BoostAsio*, const Endpoint::Options& ctrl_opts, const Endpoint::Options& data_opts) override;
  void Start() override;
  void Stop() override;
  bool IsStopped() const override;

 private:
  VideoWriterService::Ptr service_;
};

extern "C" VideoWriterPlugin* Create();
extern "C" void Destroy(VideoWriterPlugin*);
extern "C" void GetDesc(IPlugin::Desc*);

}  // namespace omv

#endif  // OMV_VIDEOWRITERPLUGIN_H
