#ifndef OMV_VIDEO_WRITER_SERVICE_H
#define OMV_VIDEO_WRITER_SERVICE_H

#include <atomic>

#include <boost/asio.hpp>
#include <opencv2/opencv.hpp>

#include "omv/proto/service.pb.h"
#include "omv/proto/video_writer.pb.h"
#include "omvnn/base_service.h"

//#include "video_writer.h"

namespace gproto = google::protobuf;
namespace asio = boost::asio;

namespace omv {

class VideoWriterService
    : public IService<proto::VideoWriter>
    , public proto::VideoWriterService {
 public:
  using Ptr = std::shared_ptr<VideoWriterService>;
  VideoWriterService();

  void Start() override;
  void Stop() override;

  void FromProto(const proto::VideoWriter*) override;
  void ToProto(proto::VideoWriter*) const override;
  void Init(BoostAsio*) override;
  void Process(VideoFrame::Ptr frame) override;

  // Google RPC Service methods
  void GetVideoWriterBackendName(::google::protobuf::RpcController* controller,
                                 const ::omv::proto::GetVideoWriterBackendNameReq* request,
                                 ::omv::proto::GetVideoWriterBackendNameRes* response,
                                 ::google::protobuf::Closure* done) override;

 private:
  cv::VideoWriter vw_;
  std::mutex vw_lock_;

  BoostAsio* io_;

  std::string path_;
  std::atomic<double> fps_;
  int32_t width_;
  int32_t height_;
  int32_t fourcc_;
  cv::VideoCaptureAPIs api_;
};

}  // namespace omv

#endif  // OMV_VIDEO_WRITER_SERVICE_H
