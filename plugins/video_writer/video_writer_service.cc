#include "video_writer_service.h"

#include <boost/asio.hpp>

#include "fmt/format.h"
#include "utils/helper.h"

namespace omv {

VideoWriterService::VideoWriterService() {
  fourcc_ = FourCC::ToInt("avc1");

  ResetStopEvent();
  SetStopEvent();
}

void VideoWriterService::FromProto(const proto::VideoWriter* proto) {
  if (!proto->path().empty())
    path_ = proto->path();
  if (proto->fps() > 0)
    fps_.store(proto->fps());
  if (proto->width())
    width_ = proto->width();
  if (proto->height())
    height_ = proto->height();
  if (proto->api())
    api_ = static_cast<cv::VideoCaptureAPIs>(proto->api());
  if (!proto->fourcc().empty() && proto->fourcc().size() == 4)
    fourcc_ = FourCC::ToInt("avc1");
  else
    LOG(ERROR) << "invalid fourcc string (wrong size): " << proto->fourcc();
}

void VideoWriterService::ToProto(proto::VideoWriter* proto) const {
  proto->mutable_path()->assign(path_.begin(), path_.end());
  proto->set_fps(fps_.load());
  proto->set_width(width_);
  proto->set_height(height_);
  proto->set_api(static_cast<proto::VideoBackend::API>(api_));
  proto->set_fourcc(FourCC::FromInt(fourcc_));
}

void VideoWriterService::Init(BoostAsio* io) { io_ = io; }
void VideoWriterService::Start() {
  if (!vw_.open(path_, api_, fourcc_, fps_, cv::Size{width_, height_}))
    throw(omv::Exception("failed opening video writer"));

  ResetStopEvent();

  LOG(INFO) << "started video writer (path: " << path_ << ')';
}
void VideoWriterService::Stop() {
  std::lock_guard<std::mutex> locker(vw_lock_);

  if (!IsStopped()) {
    SetStopEvent();

    if (vw_.isOpened())
      vw_.release();

    LOG(INFO) << "stopped video writer (path: " << path_ << ')';
  }
}

void VideoWriterService::Process(VideoFrame::Ptr frame) {
  std::lock_guard<std::mutex> locker(vw_lock_);

  if (!IsStopped())
    vw_ << frame;
}

void VideoWriterService::GetVideoWriterBackendName(google::protobuf::RpcController*,
                                                   const proto::GetVideoWriterBackendNameReq*,
                                                   proto::GetVideoWriterBackendNameRes* response,
                                                   google::protobuf::Closure*) {
  try {
    if (!vw_.isOpened())
      throw(omv::Exception(fmt::format("video writer closed: {}", path_)));

    const auto& api_name = vw_.getBackendName();

    response->set_api_name(api_name);
  } catch (const std::exception& e) {
    ErrorResponse(response, e);
  }
}

}  // namespace omv
