#include "video_writer_plugin.h"

namespace omv {

VideoWriterPlugin::VideoWriterPlugin() { service_ = std::make_shared<VideoWriterService>(); }

void VideoWriterPlugin::ServiceFromProto(const gproto::Any* service_proto) {
  proto::VideoWriter vw_proto;
  service_proto->UnpackTo(&vw_proto);
  service_->FromProto(&vw_proto);
}

void VideoWriterPlugin::ServiceToProto(gproto::Any* service_proto) const {
  proto::VideoWriter vw_proto;
  service_->ToProto(&vw_proto);
  service_proto->PackFrom(vw_proto);
}

void VideoWriterPlugin::Init(BoostAsio* io, const Endpoint::Options& ctrl_opts,
                             const Endpoint::Options& data_opts) {
  service_->Init(io);
  SetQueueCallback([&](VideoFrame::Ptr frame) { service_->Process(frame); });
  InitEndpoints(io, ctrl_opts, data_opts);
}

void VideoWriterPlugin::Start() {
  service_->Start();
  StartQueue();
  StartEndpoints();
}

void VideoWriterPlugin::Stop() {
  StopEndpoints();
  StopQueue();
  service_->Stop();
}

bool VideoWriterPlugin::IsStopped() const { return service_->IsStopped(); }

VideoWriterPlugin* Create() { return new VideoWriterPlugin; }
void Destroy(VideoWriterPlugin* plugin) { delete plugin; }
void GetDesc(IPlugin::Desc* desc) {
  desc->type = IPlugin::Type::VIDEO_WRITER;
  desc->name = "video writer";
  desc->desc = "video writer desc";
}

}  // namespace omv
