#ifndef OMV_SUPERRESOLUTIONSERVICE_H
#define OMV_SUPERRESOLUTIONSERVICE_H

//#include <boost/asio.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/opencv.hpp>

#include "fmt/format.h"
#include "omv/proto/super_resolution.pb.h"
#include "omvnn/base_service.h"

namespace omv {

class SuperResolutionService : public IService<proto::SuperResolution> {
 public:
  using Ptr = std::shared_ptr<SuperResolutionService>;
  SuperResolutionService();
  ~SuperResolutionService() override = default;

  void FromProto(const proto::SuperResolution*) override;
  void ToProto(proto::SuperResolution*) const override;
  void Init(BoostAsio*) override;
  void Start() override;
  void Stop() override;
  void Process(VideoFrame::Ptr frame) override;

 private:
  BoostAsio* io_;

  // recieve and process data
  std::mutex sp_lock_;

  // NN stuff
  cv::dnn::Net net_;
  cv::dnn::Backend backend_;
  cv::dnn::Target target_;
};

}  // namespace omv

#endif  // OMV_SUPERRESOLUTIONSERVICE_H
