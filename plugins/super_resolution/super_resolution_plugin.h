#ifndef OMV_SUPERRESOLUTIONPLUGIN_H
#define OMV_SUPERRESOLUTIONPLUGIN_H

#include "omvnn/base_plugin.h"

#include "super_resolution_service.h"

namespace omv {

class SuperResolutionPlugin : public IPlugin {
 public:
  SuperResolutionPlugin();
  ~SuperResolutionPlugin() override = default;

  void ServiceFromProto(const gproto::Any*) override;
  void ServiceToProto(gproto::Any*) const override;
  void Init(BoostAsio*, const Endpoint::Options& ctrl_opts, const Endpoint::Options& data_opts) override;
  void Start() override;
  void Stop() override;
  bool IsStopped() const override;

 private:
  SuperResolutionService::Ptr service_;
};

extern "C" SuperResolutionPlugin* Create();
extern "C" void Destroy(SuperResolutionPlugin*);
extern "C" void GetDesc(IPlugin::Desc*);

}  // namespace omv

#endif  // OMV_SUPERRESOLUTIONPLUGIN_H
