#include "super_resolution_plugin.h"

namespace omv {

SuperResolutionPlugin::SuperResolutionPlugin() {}

void SuperResolutionPlugin::ServiceFromProto(const google::protobuf::Any*) {}

void SuperResolutionPlugin::ServiceToProto(google::protobuf::Any*) const {}

void SuperResolutionPlugin::Init(IPlugin::BoostAsio*, const Endpoint::Options& ctrl_opts,
                                 const Endpoint::Options& data_opts) {}

void SuperResolutionPlugin::Start() {
  service_->Start();
  StartQueue();
  StartEndpoints();
}

void SuperResolutionPlugin::Stop() {
  StopEndpoints();
  StopQueue();
  service_->Stop();
}

bool SuperResolutionPlugin::IsStopped() const { return service_->IsStopped(); }

SuperResolutionPlugin* Create() { return new SuperResolutionPlugin; }
void Destroy(SuperResolutionPlugin* plugin) { delete plugin; }
void GetDesc(IPlugin::Desc* desc) {
  desc->type = IPlugin::Type::SUPER_RESOLUTION;
  desc->name = "super resolution";
  desc->desc = "super resolution";
}

}  // namespace omv
