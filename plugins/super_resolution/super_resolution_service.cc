#include "super_resolution_service.h"

namespace omv {
SuperResolutionService::SuperResolutionService() {
  ResetStopEvent();
  SetStopEvent();
}

void SuperResolutionService::FromProto(const proto::SuperResolution* proto) {
  if (proto->backend())
    backend_ = static_cast<cv::dnn::Backend>(proto->backend());
  if (proto->target())
    target_ = static_cast<cv::dnn::Target>(proto->target());
}

void SuperResolutionService::ToProto(proto::SuperResolution* proto) const {
  proto->set_backend(static_cast<proto::DnnBackend::Backend>(backend_));
  proto->set_target(static_cast<proto::DnnBackend::Target>(target_));
}

void SuperResolutionService::Init(IService::BoostAsio*) {
  net_ = cv::dnn::readNetFromONNX("/home/rs/Omega/cpp-va/models/super_resolution/super_resolution.onnx");
  net_.setPreferableBackend(cv::dnn::DNN_BACKEND_CUDA);
  net_.setPreferableTarget(cv::dnn::DNN_TARGET_CUDA);
}

void SuperResolutionService::Start() {
  ResetStopEvent();
  LOG(INFO) << "started super resolution";
}

void SuperResolutionService::Stop() {
  std::lock_guard<std::mutex> locker(sp_lock_);

  if (!IsStopped()) {
    SetStopEvent();
    LOG(INFO) << "stopped super resolution";
  }
}

void SuperResolutionService::Process(VideoFrame::Ptr frame) {
  auto size = frame->GetMat().size();
  cv::Mat temp;
  cv::cvtColor(frame->GetMat(), temp, cv::COLOR_BGR2YCrCb);

  cv::Mat chan[3];
  cv::split(temp, chan);

  temp = chan[0].reshape(1);

  cv::Mat temp32f;
  temp.convertTo(temp32f, CV_32FC1, 1.0 / 255);

  cv::Mat blob;
  {
    std::lock_guard<std::mutex> locker(sp_lock_);
    cv::dnn::blobFromImage(temp32f, blob, 1, cv::Size{224, 224});
    net_.setInput(blob);
    temp = net_.forward();
  }
  temp = temp.reshape(1);

  temp = temp * 255;
  temp.setTo(0, temp < 0);
  temp.setTo(255, temp > 255);

  temp.convertTo(chan[0], CV_8UC1);

  chan[0] = chan[0].reshape(1, 672);

  cv::resize(chan[1], chan[1], cv::Size(672, 672), 0, 0, cv::INTER_CUBIC);
  cv::resize(chan[2], chan[2], cv::Size(672, 672), 0, 0, cv::INTER_CUBIC);
  cv::merge(chan, 3, temp);
  cv::cvtColor(temp, temp, cv::COLOR_YCrCb2BGR);
  cv::resize(temp, temp, size, 0, 0, cv::INTER_CUBIC);
}
}  // namespace omv
