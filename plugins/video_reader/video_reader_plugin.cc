#include "video_reader_plugin.h"

#include "fmt/format.h"

namespace omv {

VideoReaderPlugin::VideoReaderPlugin() { service_ = std::make_shared<VideoReaderService>(); }

void VideoReaderPlugin::ServiceFromProto(const gproto::Any* service_proto) {
  proto::VideoReader vr_proto;
  service_proto->UnpackTo(&vr_proto);
  service_->FromProto(&vr_proto);
}

void VideoReaderPlugin::ServiceToProto(gproto::Any* service_proto) const {
  proto::VideoReader vr_proto;
  service_->ToProto(&vr_proto);
  service_proto->PackFrom(vr_proto);
}

void VideoReaderPlugin::Init(BoostAsio* io, const Endpoint::Options& ctrl_opts,
                             const Endpoint::Options& data_opts) {
  service_->Init(io);

  RegisterService(service_);
  InitEndpoints(io, ctrl_opts, data_opts);
}

void VideoReaderPlugin::Start() {
  service_->Start();
  StartEndpoints();
}

void VideoReaderPlugin::Stop() {
  StopEndpoints();
  service_->Stop();
}

bool VideoReaderPlugin::IsStopped() const { return service_->IsStopped(); }

VideoReaderPlugin* Create() { return new VideoReaderPlugin; }
void Destroy(VideoReaderPlugin* plugin) { delete plugin; }
void GetDesc(IPlugin::Desc* desc) {
  desc->type = IPlugin::Type::VIDEO_READER;
  desc->name = "video reader";
  desc->desc = "video reader desc";
  desc->has_queue = false;
}

}  // namespace omv
