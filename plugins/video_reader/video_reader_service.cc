#include "video_reader_service.h"

#include <cstdlib>
#include <filesystem>

#include "fmt/format.h"

namespace omv {

VideoReaderService::VideoReaderService() {
  ResetStopEvent();
  SetStopEvent();
}

VideoReaderService::~VideoReaderService() {
  if (vr_.isOpened())
    vr_.release();
}

void VideoReaderService::FromProto(const proto::VideoReader* proto) {
  if (!proto->path().empty())
    path_ = proto->path();
  if (proto->fps() > 0)
    fps_.store(proto->fps());
  if (proto->width())
    width_ = proto->width();
  if (proto->height())
    height_ = proto->height();
  if (proto->api())
    api_ = static_cast<cv::VideoCaptureAPIs>(proto->api());
  if (proto->cuda_device_id() > 0)
    cuda_device_id_ = proto->cuda_device_id();
  if (!proto->h26x_codec().empty())
    h26x_ = proto->h26x_codec();
}

void VideoReaderService::ToProto(proto::VideoReader* proto) const {
  proto->mutable_path()->assign(path_.begin(), path_.end());
  proto->set_fps(fps_.load());
  proto->set_width(width_);
  proto->set_height(height_);
  proto->set_api(static_cast<proto::VideoBackend::API>(api_));
  proto->set_cuda_device_id(cuda_device_id_);
  proto->set_h26x_codec(h26x_);
}

void VideoReaderService::Init(BoostAsio* io) {
  io_ = io;
  ReadFirstFrame();
}

void VideoReaderService::Process(VideoFrame::Ptr frame) { Publish(frame); }

void VideoReaderService::Start() {
  if (nullptr == io_)
    throw std::runtime_error("video reader not initialized!");

  frame_ = std::make_shared<VideoFrame>(height_, width_);
  ResetStopEvent();
  StartLoop();

  LOG(INFO) << "started video reader (path: " << path_ << ')';
}
void VideoReaderService::Stop() {
  if (!IsStopped())
    SetStopEvent();

  LOG(INFO) << "stopped video reader (path: " << path_ << ')';
}

void VideoReaderService::GetVideoReaderBackendName(google::protobuf::RpcController*,
                                                   const proto::GetVideoReaderBackendNameReq*,
                                                   proto::GetVideoReaderBackendNameRes* response,
                                                   google::protobuf::Closure*) {
  try {
    if (!vr_.isOpened())
      throw(omv::Exception(fmt::format("video reader not opened: {}", path_)));

    const auto& api_name = vr_.getBackendName();

    response->set_api_name(api_name);
  } catch (const std::exception& e) {
    ErrorResponse(response, e);
  }
}

void VideoReaderService::SetFFMpegEnvs() {
  std::string value;

  if (cuda_device_id_ > 0)
    value = fmt::format(
        "video_codec;{}_cuvid|hwaccel_device;{}|fflags;nobuffer|flags;low_latency|framedrop|framerate;{}|rtsp_"
        "transport;tcp",
        h26x_, cuda_device_id_, static_cast<int>(std::round(fps_.load())));
  else
    value = fmt::format(
        "video_codec;{}_cuvid|fflags;nobuffer|flags;low_latency|framedrop|framerate;{}|rtsp_transport;tcp", h26x_,
        static_cast<int>(std::round(fps_.load())));

  setenv("OPENCV_FFMPEG_CAPTURE_OPTIONS", value.c_str(), true);
}

void VideoReaderService::ReadFirstFrame() {
  if (api_ == cv::VideoCaptureAPIs::CAP_FFMPEG)
    SetFFMpegEnvs();

  vr_.open(path_, api_);

  namespace fs = std::filesystem;

  bool is_file = fs::is_regular_file(path_);

  cv::Mat cpu_frame;

  if (is_file)
    vr_ >> cpu_frame;

  if (!vr_.read(cpu_frame)) {
    cpu_frame.release();
    throw(omv::Exception(fmt::format("failed reading first frame (path: {}, api: {})", path_, api_)));
  }

  height_ = cpu_frame.size().height;
  width_ = cpu_frame.size().width;
}

void VideoReaderService::StartLoop() {
  io_->post([&] {
    if (!IsStopped())
      vr_ >> frame_;
    else
      return;

    if (nullptr == frame_ || frame_->IsEmpty())
      LOG(ERROR) << "failed reading next frame";
    else
      Process(frame_);

    StartLoop();
  });
}

}  // namespace omv
