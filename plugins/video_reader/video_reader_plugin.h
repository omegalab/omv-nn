#ifndef OMV_VIDEO_READER_PLUGIN_H
#define OMV_VIDEO_READER_PLUGIN_H

#include "omvnn/base_plugin.h"
#include "omvnn/base_service.h"
#include "omvnn/endpoint.h"

#include "video_reader_service.h"

namespace omv {

class VideoReaderPlugin : public IPlugin {
 public:
  VideoReaderPlugin();
  ~VideoReaderPlugin() override = default;

  void ServiceFromProto(const gproto::Any*) override;
  void ServiceToProto(gproto::Any*) const override;
  void Init(BoostAsio*, const Endpoint::Options&, const Endpoint::Options&) override;
  void Start() override;
  void Stop() override;
  bool IsStopped() const override;

 private:
  VideoReaderService::Ptr service_;
};

extern "C" VideoReaderPlugin* Create();
extern "C" void Destroy(VideoReaderPlugin*);
extern "C" void GetDesc(IPlugin::Desc*);

}  // namespace omv

#endif  // OMV_VIDEO_READER_PLUGIN_H
