#ifndef OMV_VIDEO_READER_SERVICE_H
#define OMV_VIDEO_READER_SERVICE_H

#include <atomic>

#include <boost/asio.hpp>
#include <google/protobuf/arena.h>
#include <opencv2/opencv.hpp>

#include "omv/proto/service.pb.h"
#include "omv/proto/video_reader.pb.h"
#include "omvnn/base_service.h"

namespace gproto = google::protobuf;
namespace asio = boost::asio;

namespace omv {

class VideoReaderService
    : public IService<proto::VideoReader>
    , public proto::VideoReaderService {
 public:
  using Ptr = std::shared_ptr<VideoReaderService>;

  VideoReaderService();
  ~VideoReaderService() override;

  void Start() override;
  void Stop() override;
  void FromProto(const proto::VideoReader* proto) override;
  void ToProto(proto::VideoReader* proto) const override;
  void Init(BoostAsio*) override;
  void Process(VideoFrame::Ptr frame) override;

  void GetVideoReaderBackendName(::google::protobuf::RpcController*,
                                 const ::omv::proto::GetVideoReaderBackendNameReq*,
                                 ::omv::proto::GetVideoReaderBackendNameRes*,
                                 ::google::protobuf::Closure*) override;

 private:
  cv::VideoCapture vr_;
  VideoFrame::Ptr frame_{nullptr};

  BoostAsio* io_;

  std::string path_;
  std::atomic<double> fps_{25};
  uint32_t cuda_device_id_{0};
  std::string h26x_{"h264"};
  int32_t width_;
  int32_t height_;
  cv::VideoCaptureAPIs api_;
  uint16_t failed_frames_{0};

  void SetFFMpegEnvs();
  void ReadFirstFrame();
  void StartLoop();
};

}  // namespace omv

#endif  // OMV_VIDEO_READER_SERVICE_H
