#include "queue.h"

#include "utils/helper.h"
#include "utils/logger.h"

namespace omv {

Queue::Queue() : stopped_{false} {}

Queue::~Queue() {
  if (!stopped_.load())
    Stop();

  JoinAll();
}

void Queue::Start(uint32_t max_size, uint32_t max_threads) {
  max_size_ = max_size;
  max_threads_ = max_threads;

  for (size_t i = 0; i < max_threads_; i++)
    threads_.emplace_back(std::make_unique<std::thread>(&Queue::Wait, this));

  LOG(DEBUG) << "queue started (workers count: " << threads_.size() << ')';
}

void Queue::Stop() {
  stopped_.store(true);
  cv_.notify_all();

  LOG(DEBUG) << "queue stopped";
}

void Queue::SetCallback(Callback&& callback) { callback_ = std::move(callback); }

void Queue::Add(VideoFrame::Ptr frame) {
  std::unique_lock<std::mutex> lock(lock_);

  if (queue_.size() > max_size_)
    queue_.pop();

  queue_.push(frame);
  cv_.notify_one();
}

void Queue::JoinAll() {
  for (auto&& thread : threads_) {
    if (thread->joinable())
      thread->join();
  }
}

void Queue::Wait() {
  Helper::SetThreadName("omvnnd:qu");
  std::unique_lock<std::mutex> lock(lock_);

  while (!stopped_) {
    cv_.wait(lock, [this] { return (!queue_.empty() || stopped_); });

    if (stopped_)
      break;

    if (!queue_.empty()) {
      auto frame = queue_.front();
      queue_.pop();

      lock.unlock();
      if (callback_)
        callback_(frame);
      lock.lock();
    }
  }
}

}  // namespace omv
