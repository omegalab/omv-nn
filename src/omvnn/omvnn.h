#ifndef OMV_OMVNN_H
#define OMV_OMVNN_H

#include <boost/asio.hpp>

#include "utils/logger.h"
#include "utils/options.h"

#include "endpoint.h"
#include "master_service.h"
#include "omv_version.h"
#include "registry.h"

namespace asio = boost::asio;

namespace omv {

class OmvNN {
 public:
#ifdef MODERN_BOOST
  using BoostAsio = boost::asio::io_context;
#else
  using BoostAsio = boost::asio::io_service;
#endif

  OmvNN(int argc, char** argv);
  ~OmvNN();

  void Start();
  void Stop();

 private:
  BoostAsio io_;
  asio::signal_set signals_;

  std::vector<std::unique_ptr<std::thread>> io_threads_;
  MasterService::Ptr master_service_;
  Endpoint::Ptr master_control_;
};

}  // namespace omv

#endif  // OMV_OMVNN_H
