#include "endpoint.h"

#include <cstdio>

#include "omv/proto/video_frame.pb.h"
#include "utils/options.h"

namespace omv {

Endpoint::Endpoint(BoostAsio* io, int socket_type) : socket_{*io, socket_type} {
  auto& opts = omv::Options::Instance();

  const std::string& fifo_dir = opts["fifo"];
  const std::string& bind_adr = opts["bind-adr"];

  transport_schemes_.insert({Transport::PIPE, fmt::format("ipc://{}/", fifo_dir)});
  transport_schemes_.insert({Transport::NETWORK, fmt::format("tcp://{}", bind_adr)});
  transport_schemes_.insert({Transport::INPROC, "inproc://"});

  start_stop_methods_.insert({Type::SERVER, {[&] { Bind(); }, [&] { Unbind(); }}});
  start_stop_methods_.insert({Type::CLIENT, {[&] { Connect(); }, [&] { Disconnect(); }}});
}

Endpoint::~Endpoint() {
  const auto& stop_method = start_stop_methods_.at(opts_.type).second;
  stop_method();
}

void Endpoint::Start() {
  ResetStopEvent();
  StartRead();
}

void Endpoint::Stop() { SetStopEvent(); }

void Endpoint::FromProto(const proto::Endpoint* proto) {
  opts_.role = static_cast<Role>(proto->role());
  opts_.transport = static_cast<Transport>(proto->transport());
}

void Endpoint::ToProto(proto::Endpoint* proto) const {
  proto->set_role(static_cast<proto::Endpoint::Role>(opts_.role));
  proto->set_transport(static_cast<proto::Endpoint::Transport>(opts_.transport));
  proto->mutable_address()->assign(address_.begin(), address_.end());
}

bool Endpoint::IsStopped() const {
  return stop_event_.wait_for(std::chrono::milliseconds(1)) == std::future_status::ready;
}

void Endpoint::Subscribe(const std::string& topic) { socket_.set_option(azmq::socket::subscribe(topic)); }

void Endpoint::Publish(VideoFrame::Ptr frame) {
  gproto::ArenaOptions arena_opts;
  arena_opts.initial_block_size = 3 * 1024 * 1024;

  gproto::Arena arena(arena_opts);
  proto::VideoFrame* frame_proto = gproto::Arena::CreateMessage<proto::VideoFrame>(&arena);

  frame->ToProto(frame_proto);

  size_t frame_size = frame_proto->ByteSizeLong();
  azmq::message msg(frame_size);

  frame_proto->SerializeToArray(boost::asio::buffer_cast<uchar*>(msg.buffer()), static_cast<int>(frame_size));

  socket_.send(msg);
}

std::string Endpoint::GetAddress() const { return socket_.endpoint(); }

std::string Endpoint::MakeAddress(const std::string& uuid) const {
  if (opts_.transport == Transport::NETWORK)
    throw std::logic_error(fmt::format("invalid uuid string for non-network transport: {}", uuid));

  const std::string& scheme = transport_schemes_.at(opts_.transport);
  return (opts_.role == Role::CONTROL) ? fmt::format("{}/{}-ctrl", scheme, uuid)
                                       : fmt::format("{}/{}-data", scheme, uuid);
}

std::string Endpoint::MakeAddress(uint16_t port) const {
  if (opts_.transport != Transport::NETWORK)
    throw std::logic_error(fmt::format("invalid port number for network transport: {}", port));

  const std::string& scheme = transport_schemes_.at(opts_.transport);
  return (port > 1024) ? fmt::format("{}:{}", scheme, port) : fmt::format("{}:!", scheme);
}

void Endpoint::Bind() {
  socket_.bind(address_);
  address_ = GetAddress();
  LOG(INFO) << "listening: " << address_;
}

void Endpoint::Connect() {
  socket_.connect(address_);
  address_ = GetAddress();
  LOG(INFO) << "connected to: " << address_;
}

void Endpoint::Unbind() {
  socket_.unbind(address_);

  if (opts_.transport == Transport::PIPE) {
    std::string pipe_path = address_.substr(6);
    std::remove(pipe_path.c_str());
  }
}

void Endpoint::Disconnect() {
  socket_.disconnect(address_);
  LOG(INFO) << "disconnected from: " << address_;
}

void Endpoint::StartRead() {
  if (read_handler_)
    socket_.async_receive([&](const Error& ec, azmq::message& msg, size_t bytes)  //
                          { read_handler_(ec, msg, bytes); });
}

void Endpoint::ResetStopEvent() {
  stop_signal_ = std::promise<void>();
  stop_event_ = stop_signal_.get_future();
}

void Endpoint::SetStopEvent() { stop_signal_.set_value(); }

Endpoint::Options::Options(const Endpoint::Options& other) { operator=(other); }

Endpoint::Options& Endpoint::Options::operator=(const Endpoint::Options& other) {
  type = other.type;
  role = other.role;
  transport = other.transport;
  return *this;
}

}  // namespace omv
