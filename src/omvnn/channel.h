#ifndef OMV_CHANNEL_H
#define OMV_CHANNEL_H

#include "google/protobuf/service.h"

namespace gproto = google::protobuf;

namespace omv {

class Channel : public gproto::RpcChannel {
 public:
  Channel();

  void CallMethod(const gproto::MethodDescriptor* method, gproto::RpcController* controller,
                  const gproto::Message* request, gproto::Message* response, gproto::Closure* done) override {}
};

}  // namespace omv

#endif  // OMV_CHANNEL_H
