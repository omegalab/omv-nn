#ifndef OMV_RPC_MAIN_SERVICE_H
#define OMV_RPC_MAIN_SERVICE_H

#include <boost/asio.hpp>

#include "omv/proto/service.pb.h"
#include "utils/helper.h"
#include "utils/logger.h"

#include "base_service.h"
#include "registry.h"

namespace gproto = google::protobuf;
namespace asio = boost::asio;

namespace omv {

class MasterService
    : public IService<void>
    , public proto::MasterService {
 public:
  using Ptr = std::shared_ptr<MasterService>;

  MasterService(BoostAsio& io);
  ~MasterService() override {}

  void Start() override;
  void Stop() override;

  void FromProto(const void*) override { LOG(WARNING) << "not implemented for master service"; }
  void ToProto(void*) const override { LOG(WARNING) << "not implemented for master service"; }
  void Init(BoostAsio*) override { LOG(WARNING) << "not implemented for master service"; }
  void Process(VideoFrame::Ptr) override { LOG(WARNING) << "not implemented for master service"; }

  void Destroy();

  void Ping(::google::protobuf::RpcController* controller, const ::omv::proto::PingReq* request,
            ::omv::proto::PingRes* response, ::google::protobuf::Closure* done) override;
  void CreatePlugin(::google::protobuf::RpcController* controller, const ::omv::proto::CreatePluginReq* request,
                    ::omv::proto::CreatePluginRes* response, ::google::protobuf::Closure* done) override;
  void StartPlugin(::google::protobuf::RpcController* controller, const ::omv::proto::StartPluginReq* request,
                   ::omv::proto::StartPluginRes* response, ::google::protobuf::Closure* done) override;
  void StopPlugin(::google::protobuf::RpcController* controller, const ::omv::proto::StopPluginReq* request,
                  ::omv::proto::StopPluginRes* response, ::google::protobuf::Closure* done) override;
  void DestroyPlugin(::google::protobuf::RpcController* controller, const ::omv::proto::DestroyPluginReq* request,
                     ::omv::proto::DestroyPluginRes* response, ::google::protobuf::Closure* done) override;

 private:
  BoostAsio& io_;
  Registry::Ptr registry_;

  void StartDemo(const std::string&, const std::string&);
};

}  // namespace omv

#endif  // OMG_RPC_SERVICE_H
