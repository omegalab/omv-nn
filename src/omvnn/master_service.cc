#include "master_service.h"

#include "fmt/format.h"
#include "omv/proto/dummy.pb.h"
#include "omv/proto/video_backend.pb.h"
#include "omv/proto/video_reader.pb.h"
#include "omv/proto/video_writer.pb.h"
#include "omv/proto/yolo_detector.pb.h"
#include "omvnn/base_plugin.h"
#include "utils/helper.h"
#include "utils/options.h"

namespace omv {

MasterService::MasterService(BoostAsio& io) : io_{io} { registry_ = std::make_shared<Registry>(io); }

void MasterService::Start() {
  registry_->Start();

  auto& opts = Options::Instance();
  const std::string& demo_src = opts["demo-src"];
  const std::string& demo_dst = opts["demo-dst"];

  if (!demo_src.empty() && !demo_dst.empty())
    StartDemo(demo_src, demo_dst);
}
void MasterService::Stop() { registry_->Stop(); }
void MasterService::Destroy() { registry_->Destroy(); }

void MasterService::StartDemo(const std::string& src, const std::string& dst) {
  // Set up all the neccessary proto stuff to create vr/vw/yolo plugins
  gproto::Arena arena;

  proto::CreatePluginReq* request = gproto::Arena::CreateMessage<proto::CreatePluginReq>(&arena);
  proto::CreatePluginRes* response = gproto::Arena::CreateMessage<proto::CreatePluginRes>(&arena);
  proto::VideoReader* vr_proto = gproto::Arena::CreateMessage<proto::VideoReader>(&arena);
  proto::VideoWriter* vw_proto = gproto::Arena::CreateMessage<proto::VideoWriter>(&arena);
  proto::YoloDetector* yd_proto = gproto::Arena::CreateMessage<proto::YoloDetector>(&arena);
  proto::Dummy* dummy_proto = gproto::Arena::CreateMessage<proto::Dummy>(&arena);

  proto::Plugin* plugin_proto = request->mutable_plugin();
  proto::Endpoint* ctrl_e_proto = plugin_proto->mutable_control();
  proto::Endpoint* data_e_proto = plugin_proto->mutable_data_out();

  // Creating dummy plugin
  plugin_proto->mutable_service()->PackFrom(*dummy_proto);
  plugin_proto->mutable_desc()->set_type(proto::Plugin::DUMMY);

  ctrl_e_proto->set_transport(proto::Endpoint::INPROC);
  ctrl_e_proto->set_role(proto::Endpoint::CONTROL);
  data_e_proto->set_transport(proto::Endpoint::INPROC);
  data_e_proto->set_role(proto::Endpoint::DATA);

  CreatePlugin(nullptr, request, response, nullptr);

  LOG(INFO) << response->DebugString();

  const std::string& dummy_plugin_uuid = response->plugin().uuid();
  auto dummy_plugin = registry_->GetPlugin(dummy_plugin_uuid);

  // Creating vr plugin and adding it to registy
  vr_proto->set_fps(25);
  vr_proto->set_h26x_codec("h264");
  vr_proto->set_api(proto::VideoBackend::CAP_FFMPEG);
  vr_proto->mutable_path()->assign(src.begin(), src.end());

  plugin_proto->mutable_service()->PackFrom(*vr_proto);
  plugin_proto->mutable_desc()->set_type(proto::Plugin::VIDEO_READER);

  CreatePlugin(nullptr, request, response, nullptr);

  LOG(DEBUG) << response->DebugString();

  const std::string& vr_plugin_uuid = response->plugin().uuid();
  auto vr_plugin = registry_->GetPlugin(vr_plugin_uuid);

  if (!vr_plugin) {
    LOG(ERROR) << "failed start in demo mode!";
    return;
  }

  // get resolution from VR
  vr_plugin->ToProto(plugin_proto);
  plugin_proto->service().UnpackTo(vr_proto);

  int width = vr_proto->width();
  int height = vr_proto->height();
  double fps = vr_proto->fps();
  LOG(INFO) << "resolution: " << width << " x " << height << "; fps: " << fps;

  // Creating vw plugin and adding it to registy
  size_t bitrate = 2048;

  if (width * height > 921'600)
    bitrate = 3072;

  std::vector<std::string> dst_vec = Helper::SplitString(dst, ':');

  if (dst_vec.size() != 2)
    throw std::invalid_argument("bad demo dst host (not in 'host:port' format)!");

  const std::string& dst_host = dst_vec.at(0);
  const std::string& dst_port = dst_vec.at(1);
  const std::string& gst_pipeline = fmt::format(
      "appsrc is-live=true ! videoconvert ! nvh264enc bitrate={} rc-mode=2 ! "
      "rtph264pay mtu=1316 config-interval=10 ! udpsink host={} port={} sync=false",
      bitrate, dst_host, dst_port);

  vw_proto->set_api(proto::VideoBackend::CAP_GSTREAMER);
  vw_proto->mutable_path()->assign(gst_pipeline.begin(), gst_pipeline.end());

  vw_proto->set_width(width);
  vw_proto->set_height(height);
  vw_proto->set_fps(fps);
  vw_proto->set_fourcc("avc1");

  plugin_proto->mutable_service()->PackFrom(*vw_proto);
  plugin_proto->mutable_desc()->set_type(proto::Plugin::VIDEO_WRITER);

  CreatePlugin(nullptr, request, response, nullptr);

  LOG(DEBUG) << response->DebugString();

  const std::string& vw_plugin_uuid = response->plugin().uuid();
  auto vw_plugin = registry_->GetPlugin(vw_plugin_uuid);

  // Creating yolo plugin and adding it to registy
  yd_proto->set_data_path("yolo_detector");
  yd_proto->set_width(width);
  yd_proto->set_height(height);
  yd_proto->set_confidence(0.80f);
  yd_proto->set_nms_thresh(0.4f);
  yd_proto->set_num_classes(79);
  yd_proto->set_backend(proto::DnnBackend::DNN_BACKEND_CUDA);
  yd_proto->set_target(proto::DnnBackend::DNN_TARGET_CUDA);
  yd_proto->set_width(width);
  yd_proto->set_height(height);

  plugin_proto->mutable_service()->PackFrom(*yd_proto);
  plugin_proto->mutable_desc()->set_type(proto::Plugin::YOLO_DETECTOR);

  CreatePlugin(nullptr, request, response, nullptr);

  LOG(DEBUG) << response->DebugString();

  const std::string& yolo_plugin_uuid = response->plugin().uuid();
  auto yd_plugin = registry_->GetPlugin(yolo_plugin_uuid);

  //  dummy_plugin->Connect(vr_plugin, &io_);
  vw_plugin->Connect(dummy_plugin, &io_);
  vw_plugin->Connect(yd_plugin, &io_);
  yd_plugin->Connect(vr_plugin, &io_);
  dummy_plugin->Start();
  vw_plugin->Start();
  yd_plugin->Start();
  vr_plugin->Start();
}

void MasterService::Ping(google::protobuf::RpcController* controller, const proto::PingReq* request,
                         proto::PingRes* response, google::protobuf::Closure* done) {
  try {
    throw std::runtime_error("not implemented");
  } catch (const std::exception& e) {
    ErrorResponse(response, e);
  }
}

void MasterService::CreatePlugin(google::protobuf::RpcController* controller,
                                 const proto::CreatePluginReq* request, proto::CreatePluginRes* response,
                                 google::protobuf::Closure* done) {
  try {
    gproto::Arena* arena = request->GetArena();
    proto::Plugin* plugin_proto = gproto::Arena::CreateMessage<proto::Plugin>(arena);

    plugin_proto->CopyFrom(request->plugin());

    IPlugin::Type plugin_type = static_cast<IPlugin::Type>(plugin_proto->desc().type());
    IPlugin::Ptr plugin = registry_->CreatePlugin(plugin_type);

    if (nullptr == plugin)
      throw std::runtime_error(fmt::format("no such plugin: {}", static_cast<int>(plugin_proto->desc().type())));

    Endpoint::Options ctrl_opts;
    Endpoint::Options data_opts;

    proto::Endpoint::Transport ctrl_transport = plugin_proto->control().transport();
    proto::Endpoint::Transport data_transport = plugin_proto->data_out().transport();

    ctrl_opts.transport = static_cast<Endpoint::Transport>(ctrl_transport);
    data_opts.transport = static_cast<Endpoint::Transport>(data_transport);

    ctrl_opts.type = Endpoint::Type::SERVER;
    ctrl_opts.role = Endpoint::Role::CONTROL;

    data_opts.type = Endpoint::Type::SERVER;
    data_opts.role = Endpoint::Role::DATA;

    plugin->FromProto(plugin_proto);
    plugin->Init(&io_, ctrl_opts, data_opts);

    plugin_proto->Clear();
    plugin->ToProto(plugin_proto);

    registry_->LoadPlugin(plugin);
    response->mutable_plugin()->CopyFrom(*plugin_proto);

  } catch (const std::exception& e) {
    ErrorResponse(response, e);
  }
}

void MasterService::StartPlugin(google::protobuf::RpcController* controller, const proto::StartPluginReq* request,
                                proto::StartPluginRes* response, google::protobuf::Closure* done) {
  try {
    const auto& plugin_uuid = request->uuid();
    auto plugin = registry_->GetPlugin(plugin_uuid);

    if (!plugin)
      throw(omv::Exception(fmt::format("no such plugin: {}", plugin_uuid)));

    plugin->Start();
    response->mutable_success()->set_text(fmt::format("plugin started (uuid: {})", plugin_uuid));

  } catch (const std::exception& e) {
    ErrorResponse(response, e);
  }
}

void MasterService::StopPlugin(google::protobuf::RpcController* controller, const proto::StopPluginReq* request,
                               proto::StopPluginRes* response, google::protobuf::Closure* done) {
  try {
    const auto& plugin_uuid = request->uuid();
    auto plugin = registry_->GetPlugin(plugin_uuid);

    if (!plugin)
      throw(omv::Exception(fmt::format("no such plugin: {}", plugin_uuid)));

    plugin->Stop();
    response->mutable_success()->set_text(fmt::format("plugin stopped (uuid: {})", plugin_uuid));

  } catch (const std::exception& e) {
    ErrorResponse(response, e);
  }
}

void MasterService::DestroyPlugin(google::protobuf::RpcController* controller,
                                  const proto::DestroyPluginReq* request, proto::DestroyPluginRes* response,
                                  google::protobuf::Closure* done) {
  try {
    const auto& plugin_uuid = request->uuid();
    auto plugin = registry_->GetPlugin(plugin_uuid);

    if (!plugin)
      throw(omv::Exception(fmt::format("no such plugin: {}", plugin_uuid)));

    if (!plugin->IsStopped())
      plugin->Stop();

    registry_->UnloadPlugin(plugin_uuid);
    response->mutable_success()->set_text(fmt::format("plugin unloaded (uuid: {})", plugin_uuid));

  } catch (const std::exception& e) {
    ErrorResponse(response, e);
  }
}

}  // namespace omv
