#ifndef OMV_ISERVICE_H
#define OMV_ISERVICE_H

#include <filesystem>
#include <functional>
#include <future>
#include <memory>

#include <boost/asio.hpp>
#include <boost/signals2.hpp>
#include <google/protobuf/any.pb.h>

#include "utils/helper.h"
#include "utils/logger.h"

#include "proto_wrapper.h"
#include "video_frame.h"

namespace asio = boost::asio;
namespace gproto = google::protobuf;

namespace omv {

template <typename T>
class IService : public ProtoWrapper<T> {
 public:
#ifdef MODERN_BOOST
  using BoostAsio = boost::asio::io_context;
#else
  using BoostAsio = boost::asio::io_service;
#endif

  using Ptr = std::shared_ptr<IService>;
  IService() = default;

  virtual ~IService() = default;
  virtual void Init(BoostAsio*) = 0;
  virtual void Start() = 0;
  virtual void Stop() = 0;
  virtual void Process(VideoFrame::Ptr) = 0;

  bool IsStopped() const {
    return stop_event_.wait_for(std::chrono::milliseconds(1)) == std::future_status::ready;
  }
  void ConnectToProcessed(std::function<void(VideoFrame::Ptr)>&& func) { on_processed_.connect(std::move(func)); }
  void Publish(VideoFrame::Ptr frame) {
    if (!on_processed_.empty())
      on_processed_(frame);
    else
      LOG(ERROR) << "on_prcessed signal is empty!";
  }

  template <typename R>
  void ErrorResponse(R* response, const std::exception& e) {
    const std::string& e_text = e.what();

    response->mutable_error()->set_code(1);
    response->mutable_error()->set_text(e_text);

    LOG(ERROR) << e_text;
  }

 protected:
  void ResetStopEvent() {
    stop_signal_ = std::promise<void>();
    stop_event_ = stop_signal_.get_future();
  }
  void SetStopEvent() { stop_signal_.set_value(); }

 private:
  boost::signals2::signal<void(VideoFrame::Ptr)> on_processed_;
  std::promise<void> stop_signal_;
  std::future<void> stop_event_;
};

}  // namespace omv

#endif  // OMV_ISERVICE_H
