#ifndef OMG_DVR_H
#define OMG_DVR_H

#include <map>
#include <string>

#include <boost/asio.hpp>

#include "base_plugin.h"
#include "base_service.h"
#include "video_frame.h"

namespace asio = boost::asio;

namespace omv {

class Registry {
 public:
#ifdef MODERN_BOOST
  using BoostAsio = boost::asio::io_context;
#else
  using BoostAsio = boost::asio::io_service;
#endif

  using Ptr = std::shared_ptr<Registry>;
  using CreateDesc = std::pair<IPlugin::Desc, IPlugin::CreateT>;
  Registry(BoostAsio& io);

  void Start();
  void Stop();
  void Destroy();

  void LoadPlugin(IPlugin::Ptr plugin);
  void UnloadPlugin(const std::string& uuid);

  IPlugin::Ptr CreatePlugin(IPlugin::Type type);
  IPlugin::Ptr GetPlugin(const std::string& uuid);

 private:
  BoostAsio& io_;
  std::map<IPlugin::Type, CreateDesc> create_handlers_;
  std::map<IPlugin::Type, IPlugin::DestroyT> destroy_handlers_;

  std::map<std::string, IPlugin::Ptr> loaded_plugins_;

  void LoadHandlers();
};

}  // namespace omv

#endif  // OMG_DVR_H
