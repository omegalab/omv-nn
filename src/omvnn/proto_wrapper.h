#ifndef OMV_RPC_PROTOWRAPPER_H
#define OMV_RPC_PROTOWRAPPER_H

namespace omv {

template <typename Proto>
class ProtoWrapper {
 public:
  ProtoWrapper() = default;
  virtual ~ProtoWrapper() = default;

  virtual void FromProto(const Proto*) = 0;
  virtual void ToProto(Proto*) const = 0;
};

}  // namespace omv

#endif  // OMV_RPC_PROTOWRAPPER_H
