#include "video_frame.h"

#include <chrono>

#include <google/protobuf/arena.h>

#include "fmt/format.h"
#include "omv/proto/matrix.pb.h"
#include "omv/proto/video_frame.pb.h"
#include "utils/helper.h"
#include "utils/logger.h"

namespace omv {

VideoFrame::VideoFrame() { ResetWriteEvent(); }

VideoFrame::VideoFrame(int rows, int cols) : m_{cv::Mat::zeros(rows, cols, CV_32F)}, rows_{rows}, cols_{cols} {
  ResetWriteEvent();
}

void VideoFrame::FromProto(const proto::VideoFrame* proto) {
  //  proto::Matrix* matrix_proto = proto->mutable_matrix();
  const proto::Matrix& matrix_proto = proto->matrix();

  ts_start_ = proto->ts_start().seconds() * 1'000 + proto->ts_start().nanos() / 1'000'000;
  ts_end_ = proto->ts_end().seconds() * 1'000 + proto->ts_end().nanos() / 1'000'000;

  m_.create(matrix_proto.rows(), matrix_proto.cols(), matrix_proto.elm_type());
  int m_size = matrix_proto.rows() * matrix_proto.cols() * matrix_proto.elm_size();

  uchar* matrix_proto_data = reinterpret_cast<uchar*>(const_cast<char*>(matrix_proto.data().data()));
  std::copy(matrix_proto_data, matrix_proto_data + m_size, m_.data);
}

void VideoFrame::ToProto(proto::VideoFrame* proto) const {
  size_t m_size = m_.total() * m_.elemSize();

  Helper::SetTimeStamp(proto->mutable_ts_start(), ts_start_);
  Helper::SetTimeStamp(proto->mutable_ts_end(), ts_end_);

  proto->mutable_matrix()->set_rows(m_.rows);
  proto->mutable_matrix()->set_cols(m_.cols);
  proto->mutable_matrix()->set_elm_type(m_.type());
  proto->mutable_matrix()->set_elm_size(static_cast<int>(m_.elemSize()));
  proto->mutable_matrix()->mutable_data()->assign(m_.data, m_.data + m_size);
}

void VideoFrame::DisplayFPS() {
  float fps = 1000.f / (ts_end_ - ts_start_);
  //  double te = static_cast<double>(ts_end_ - ts_start_) / cv::getTickFrequency();
  //  double fps = 1 / te;

  const std::string& label = fmt::format("{:.{}f}", fps, 2);
  const cv::Size& text_size = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.7, 2, nullptr);

  cv::putText(m_, label,                                      //
              cv::Point(m_.cols - text_size.width - 10, 30),  //
              cv::FONT_HERSHEY_SIMPLEX, 0.7, cv::Scalar(0, 255, 0), 2);
}

bool VideoFrame::NotWritten() const {
  return write_event_.wait_for(std::chrono::milliseconds(1)) != std::future_status::ready;
}

void VideoFrame::ResetWriteEvent() {
  write_signal_ = std::promise<void>();
  write_event_ = write_signal_.get_future();
}

cv::VideoCapture& operator>>(cv::VideoCapture& vr, VideoFrame::Ptr frame) {
  //  frame->ts_start_ = cv::getTickCount();
  //  vr >> frame->m_;
  //  frame->ts_start_ = Helper::GetTimeStamp();
  return vr >> frame->m_;
}
cv::VideoWriter& operator<<(cv::VideoWriter& vw, VideoFrame::Ptr frame) {
  //  frame->ts_end_ = cv::getTickCount();
  //  frame->ts_end_ = Helper::GetTimeStamp();
  //  frame->DisplayFPS();
  return vw << frame->m_;
}

}  // namespace omv
