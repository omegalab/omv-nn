#ifndef OMV_BASE_PLUGIN_H
#define OMV_BASE_PLUGIN_H

#include <map>
#include <memory>

#include <boost/asio.hpp>
#include <google/protobuf/message.h>

#include "omv/proto/plugin.pb.h"

#include "base_service.h"
#include "endpoint.h"
#include "proto_wrapper.h"
#include "queue.h"

namespace asio = boost::asio;
namespace gproto = google::protobuf;

namespace omv {

class IPlugin : public ProtoWrapper<proto::Plugin> {
 public:
#ifdef MODERN_BOOST
  using BoostAsio = boost::asio::io_context;
#else
  using BoostAsio = boost::asio::io_service;
#endif

  enum class Type {
    UNDEFINED = 0,
    VIDEO_READER,
    VIDEO_WRITER,
    PRE_PROCESSOR,
    POST_PROCESSOR,
    DUMMY,
    YOLO_DETECTOR = 21,
    FACE_DETECTOR_OPENCV,
    SUPER_RESOLUTION,
    LICPLATES_DETECTOR
  };

  struct Desc {
    Type type{Type::UNDEFINED};
    std::string stem;
    std::string name;
    std::string desc;
    bool has_queue{true};
  };

  using CreateT = std::function<IPlugin*()>;
  using DestroyT = std::function<void(IPlugin*)>;
  using Ptr = std::shared_ptr<IPlugin>;
  using DataSlot = std::function<void(VideoFrame::Ptr)>;

  IPlugin();
  virtual ~IPlugin() override = default;

  virtual void ServiceFromProto(const gproto::Any*) = 0;
  virtual void ServiceToProto(gproto::Any*) const = 0;
  virtual void Init(BoostAsio*, const Endpoint::Options& ctrl_opts, const Endpoint::Options& data_opts) = 0;
  virtual void Start() = 0;
  virtual void Stop() = 0;
  virtual bool IsStopped() const = 0;

  void FromProto(const proto::Plugin* plugin_proto) override;
  void ToProto(proto::Plugin* plugin_proto) const override;
  void InitEndpoints(BoostAsio* io, const Endpoint::Options& ctrl_opts, const Endpoint::Options& data_opts);

  std::string GetUuid() const;
  const Desc* GetDesc() const;
  void SetDesc(const Desc*);

  void CreateControlEndpoint(BoostAsio* io);
  void CreateDataOutEndpoint(BoostAsio* io);

  Endpoint::Ptr GetDataOutEndpoint() const { return data_out_; }
  Endpoint::Ptr GetControlEndpoint() const { return control_; }

  void Publish(VideoFrame::Ptr frame);

  template <typename S>
  void RegisterService(const S& service) {
    service->ConnectToProcessed([&](VideoFrame::Ptr frame) { Publish(frame); });
  }

  void StartQueue(uint32_t max_size = 4, uint32_t max_threads = 2);
  void StopQueue();
  void SetQueueCallback(Queue::Callback&& cb);
  void Connect(IPlugin::Ptr, BoostAsio*);

  friend inline std::ostream& operator<<(std::ostream& os, const IPlugin& pl) {
    // clang-format off
    return os << pl.desc_.name
              << " (type: " << static_cast<int>(pl.desc_.type)
              << "; uuid:" << pl.uuid_ << ')';
    // clang-format on
  }

 protected:
  void StartEndpoints();
  void StopEndpoints();
  void PackEndpoints(proto::Plugin* plugin_proto) const;

 private:
  Desc desc_;

  std::string uuid_;
  ssize_t cuda_devid_{-1};

  Queue::Ptr queue_;
  Endpoint::Ptr control_;
  Endpoint::Ptr data_out_;
  Endpoint::Ptr data_in_;

  void CreateQueue();
};

}  // namespace omv

#endif  // OMV_BASE_PLUGIN_H
