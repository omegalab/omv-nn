#ifndef OMV_VIDEOFRAME_H
#define OMV_VIDEOFRAME_H

#include <atomic>
#include <future>

#include <opencv2/opencv.hpp>

#include "omv/proto/video_frame.pb.h"
#include "utils/logger.h"

#include "proto_wrapper.h"
namespace gproto = google::protobuf;

namespace omv {

class VideoFrame : public ProtoWrapper<proto::VideoFrame> {
 public:
  using Ptr = std::shared_ptr<VideoFrame>;

  cv::Mat m_;

  VideoFrame();
  VideoFrame(int rows, int cols);

  ~VideoFrame() override { m_.release(); }

  void FromProto(const proto::VideoFrame*) override;
  void ToProto(proto::VideoFrame*) const override;
  void Display(const std::string& window_name) { cv::imshow(window_name, m_); }

  bool IsEmpty() const { return m_.empty(); }
  void DisplayFPS();

  cv::Mat& GetMat() { return m_; }

  bool NotWritten() const;
  void SetWritten() { write_signal_.set_value(); }

  friend cv::VideoCapture& operator>>(cv::VideoCapture& vr, Ptr frame);
  friend cv::VideoWriter& operator<<(cv::VideoWriter& vw, Ptr frame);

 private:
  int rows_;
  int cols_;
  int64_t ts_start_{0};
  int64_t ts_end_{0};

  std::promise<void> write_signal_;
  std::shared_future<void> write_event_;

  void ResetWriteEvent();
};

}  // namespace omv

#endif  // OMV_VIDEOFRAME_H
