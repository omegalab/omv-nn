#include "base_plugin.h"

#include "fmt/format.h"
#include "utils/helper.h"

namespace omv {

IPlugin::IPlugin() : desc_{}, control_{nullptr}, data_out_{nullptr}, data_in_{nullptr} {
  uuid_ = Helper::GenerateUuid4();
  CreateQueue();
}

void IPlugin::FromProto(const proto::Plugin* plugin_proto) {
  if (queue_)
    queue_->FromProto(&plugin_proto->queue());

  //  info_.type = static_cast<IPlugin::Type>(plugin_proto->type());

  const gproto::Any& service_proto = plugin_proto->service();
  ServiceFromProto(&service_proto);
}

void IPlugin::ToProto(proto::Plugin* plugin_proto) const {
  if (queue_)
    queue_->ToProto(plugin_proto->mutable_queue());

  gproto::Any* service_proto = plugin_proto->mutable_service();
  plugin_proto->mutable_desc()->set_type(static_cast<proto::Plugin::Type>(desc_.type));
  plugin_proto->set_uuid(uuid_);
  plugin_proto->set_is_started(!IsStopped());

  ServiceToProto(service_proto);
  PackEndpoints(plugin_proto);
}

void IPlugin::InitEndpoints(IPlugin::BoostAsio* io, const Endpoint::Options& ctrl_opts,
                            const Endpoint::Options& data_opts) {
  auto endpoint_init = [&](Endpoint::Ptr endpoint, const Endpoint::Options& opts) {
    if (opts.transport == Endpoint::Transport::NETWORK)
      endpoint->Init(opts, 0);
    else
      endpoint->Init(opts, uuid_);
  };

  if (ctrl_opts.transport != Endpoint::Transport::UNDEFINED) {
    CreateControlEndpoint(io);
    endpoint_init(control_, ctrl_opts);
  }

  if (data_opts.transport != Endpoint::Transport::UNDEFINED) {
    CreateDataOutEndpoint(io);
    endpoint_init(data_out_, data_opts);
  }
}

string IPlugin::GetUuid() const { return uuid_; }
const IPlugin::Desc* IPlugin::GetDesc() const { return &desc_; }
void IPlugin::SetDesc(const IPlugin::Desc* desc) { desc_ = *desc; }

void IPlugin::CreateControlEndpoint(BoostAsio* io) { control_ = std::make_shared<Endpoint>(io, ZMQ_ROUTER); }
void IPlugin::CreateDataOutEndpoint(BoostAsio* io) { data_out_ = std::make_shared<Endpoint>(io, ZMQ_PUB); }

void IPlugin::Publish(VideoFrame::Ptr frame) { data_out_->Publish(frame); }

void IPlugin::StartQueue(uint32_t max_size, uint32_t max_threads) {
  if (nullptr != queue_)
    queue_->Start(max_size, max_threads);
}

void IPlugin::StopQueue() {
  if (nullptr != queue_)
    queue_->Stop();
}

void IPlugin::SetQueueCallback(Queue::Callback&& cb) { queue_->SetCallback(std::move(cb)); }

void IPlugin::Connect(IPlugin::Ptr plugin, BoostAsio* io) {
  Endpoint::Ptr source_endpoint = plugin->GetDataOutEndpoint();
  const IPlugin::Desc* src_plugin_desc = plugin->GetDesc();
  const std::string& src_plugin_uuid = plugin->GetUuid();

  if (source_endpoint == nullptr)
    throw std::logic_error(fmt::format("no data endpoint for {} ({})", src_plugin_desc->name, src_plugin_uuid));

  const std::string& dst = src_plugin_uuid;

  data_in_ = std::make_shared<Endpoint>(io, ZMQ_SUB);

  Endpoint::Options opts = source_endpoint->GetOptions();

  opts.type = Endpoint::Type::CLIENT;

  data_in_->Init(opts, dst);
  data_in_->Subscribe();
  data_in_->SetSndTimeo();
  data_in_->SetQueue(queue_);
  data_in_->RegisterServiceAsData();
  data_in_->Start();
}

void IPlugin::StartEndpoints() {
  if (control_)
    control_->Start();

  if (data_out_)
    data_out_->Start();
}

void IPlugin::StopEndpoints() {
  if (control_)
    control_->Stop();

  if (data_in_)
    data_in_->Stop();

  if (data_out_)
    data_out_->Stop();
}

void IPlugin::PackEndpoints(proto::Plugin* plugin_proto) const {
  if (control_)
    control_->ToProto(plugin_proto->mutable_control());

  if (data_out_)
    data_out_->ToProto(plugin_proto->mutable_data_out());
}

void IPlugin::CreateQueue() { queue_ = std::make_shared<Queue>(); }

}  // namespace omv
