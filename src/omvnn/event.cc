#include "event.h"

namespace omv {

Event::Event(BoostAsio& io) : timer_(io) { timer_.expires_at(boost::posix_time::pos_infin); }

void Event::NotifyOne() { timer_.cancel_one(); }
void Event::NotifyAll() { timer_.cancel(); }

}  // namespace omv
