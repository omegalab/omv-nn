#ifndef OMV_QUEUE_H
#define OMV_QUEUE_H

#include <atomic>
#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>

#include "omv/proto/queue.pb.h"

#include "proto_wrapper.h"
#include "video_frame.h"

namespace omv {

class Queue : public ProtoWrapper<proto::Queue> {
 public:
  using Ptr = std::shared_ptr<Queue>;
  using Callback = std::function<void(VideoFrame::Ptr)>;
  using ThreadPtr = std::unique_ptr<std::thread>;

  Queue();
  ~Queue();

  void Start(uint32_t, uint32_t);
  void Stop();
  void SetCallback(Callback&&);
  void Add(VideoFrame::Ptr);

  void FromProto(const proto::Queue* proto) {
    if (proto->max_size())
      max_size_ = proto->max_size();
    if (proto->max_threads())
      max_threads_ = proto->max_threads();
  }
  void ToProto(proto::Queue* proto) const {
    proto->set_max_size(max_size_);
    proto->set_max_threads(max_threads_);
  }

 private:
  std::atomic_bool stopped_;
  std::vector<ThreadPtr> threads_;
  std::queue<VideoFrame::Ptr> queue_;
  std::condition_variable cv_;
  std::mutex lock_;

  uint32_t max_size_{4};
  uint32_t max_threads_{2};

  Callback callback_;

  void JoinAll();
  void Wait();
};

}  // namespace omv

#endif  // OMV_QUEUE_H
