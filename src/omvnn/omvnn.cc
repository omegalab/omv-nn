#include "omvnn.h"

#include <opencv2/core/utils/logger.hpp>

namespace omv {

OmvNN::OmvNN(int argc, char** argv) : io_{}, signals_{io_} {
  cv::utils::logging::setLogLevel(cv::utils::logging::LOG_LEVEL_WARNING);
  Options::Instance().Init(argc, argv);

  signals_.add(SIGINT);
  signals_.add(SIGTERM);
  signals_.add(SIGQUIT);
  signals_.add(SIGHUP);

  signals_.async_wait([&](const boost::system::error_code& ec, int sig) {
    if (ec)
      throw std::runtime_error(ec.message());

    LOG(INFO) << "received signal: " << sig;

    if (sig != SIGHUP)
      Stop();
  });

  master_service_ = std::make_shared<MasterService>(io_);
  master_control_ = std::make_shared<Endpoint>(&io_, ZMQ_ROUTER);

  Endpoint::Options control_opts;

  control_opts.role = Endpoint::Role::CONTROL;
  control_opts.type = Endpoint::Type::SERVER;
  control_opts.transport = Endpoint::Transport::NETWORK;

  master_control_->Init(control_opts, 5556);
  master_control_->RegisterServiceAsRPC(master_service_);
}

OmvNN::~OmvNN() {
  for (auto& io_thread : io_threads_)
    io_thread->join();
}

void OmvNN::Start() {
  master_service_->Start();
  master_control_->Start();

  auto& opts = Options::Instance();
  int total_threads = opts["threads"];

  for (int i = 0; i < total_threads; ++i)
    io_threads_.emplace_back(std::make_unique<std::thread>([&] {
      Helper::SetThreadName("omvnnd:io");
      io_.run();
    }));

  LOG(INFO) << PROJECT_NAME << " started (version: " << PROJECT_VER << "; build: " << PROJECT_BUILD_DATE << ')';
}

void OmvNN::Stop() {
  master_control_->Stop();  // TODO: check if we need here to reset control and service
  master_service_->Stop();
  master_service_->Destroy();

  io_.stop();

  LOG(INFO) << PROJECT_NAME << " stopped (version: " << PROJECT_VER << "; build: " << PROJECT_BUILD_DATE << ')';
}

}  // namespace omv
