#include "registry.h"

#include <dlfcn.h>

#include <cstdlib>
#include <filesystem>

#include "utils/options.h"

namespace omv {

Registry::Registry(BoostAsio& io) : io_{io} {}

void Registry::Start() { LoadHandlers(); }

void Registry::Stop() {
  for (auto& [uuid, plugin] : loaded_plugins_)
    plugin->Stop();
}

void Registry::Destroy() { loaded_plugins_.clear(); }

IPlugin::Ptr Registry::CreatePlugin(IPlugin::Type type) {
  IPlugin::Ptr plugin = nullptr;

  if (create_handlers_.count(type)) {
    auto create_desc = create_handlers_.at(type);
    auto plugin_desc = create_desc.first;
    auto create_hndl = create_desc.second;
    plugin = std::shared_ptr<IPlugin>(create_hndl());
    plugin->SetDesc(&plugin_desc);
  }

  return plugin;
}

void Registry::LoadPlugin(IPlugin::Ptr plugin) {
  const std::string& plugin_uuid = plugin->GetUuid();
  loaded_plugins_.insert({plugin_uuid, plugin});

  LOG(INFO) << "loaded plugin: " << *plugin;
}

void Registry::LoadHandlers() {
  namespace fs = std::filesystem;
  const auto& opts = Options::Instance();

  fs::path plugins_path;

  const char* plugins_path_env = std::getenv("OMVNN_PLUGINS_PATH");

  if (nullptr != plugins_path_env)
    plugins_path = plugins_path_env;
  else if (opts.Get<std::string>("plugins").empty())
    plugins_path = fs::current_path();
  else
    plugins_path = fs::path(opts.Get<std::string>("plugins"));

  for (const auto& entry : fs::directory_iterator(plugins_path))
    if (entry.is_regular_file() && entry.path().extension() == ".so") {
      const fs::path plugin_path = entry.path();
      void* handle = dlopen(plugin_path.c_str(), RTLD_LAZY);

      if (!handle)
        LOG(ERROR) << dlerror();

      using desc_t = void(IPlugin::Desc*);
      using create_t = IPlugin*();
      using destroy_t = void(IPlugin*);

      desc_t* get_desc = reinterpret_cast<desc_t*>(dlsym(handle, "GetDesc"));
      create_t* create = reinterpret_cast<create_t*>(dlsym(handle, "Create"));
      destroy_t* destroy = reinterpret_cast<destroy_t*>(dlsym(handle, "Destroy"));

      if (!create)
        LOG(ERROR) << dlerror();

      if (!destroy)
        LOG(ERROR) << dlerror();

      IPlugin::Desc plugin_desc;
      get_desc(&plugin_desc);

      const fs::path plugin_filename = plugin_path.filename();
      plugin_desc.stem = plugin_filename;

      create_handlers_.insert({plugin_desc.type, {plugin_desc, static_cast<create_t*>(create)}});
      destroy_handlers_.insert({plugin_desc.type, static_cast<destroy_t*>(destroy)});

      // clang-format off
      LOG(INFO) << "found " << plugin_filename
                << " plugin (" << plugin_desc.name
                <<"; type: " << static_cast<int>(plugin_desc.type) << ')';
      // clang-format on
    }
}

void Registry::UnloadPlugin(const std::string& uuid) {
  if (loaded_plugins_.count(uuid))
    loaded_plugins_.erase(uuid);
  else
    throw std::runtime_error(fmt::format("plugin not loaded uuid: {}", uuid));
}

IPlugin::Ptr Registry::GetPlugin(const std::string& uuid) {
  return (loaded_plugins_.count(uuid) ? loaded_plugins_.at(uuid) : nullptr);
}

}  // namespace omv
