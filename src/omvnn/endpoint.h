#ifndef OMV_ENDPOINT_H
#define OMV_ENDPOINT_H

#include <functional>
#include <future>
#include <map>
#include <memory>
#include <type_traits>
#include <utility>

#include <boost/asio.hpp>
#include <google/protobuf/arena.h>

#include "azmq/socket.hpp"
#include "fmt/format.h"
#include "omv/proto/endpoint.pb.h"
#include "omv/proto/packet.pb.h"
#include "utils/logger.h"
#include "utils/profile.h"

#include "proto_wrapper.h"
#include "queue.h"
#include "video_frame.h"

namespace asio = boost::asio;
namespace gproto = google::protobuf;

namespace omv {

class Endpoint : public ProtoWrapper<proto::Endpoint> {
 public:
#ifdef MODERN_BOOST
  using BoostAsio = boost::asio::io_context;
#else
  using BoostAsio = boost::asio::io_service;
#endif

  enum class Type { UNDEFINED = 0, SERVER, CLIENT };
  enum class Role { UNDEFINED = 0, CONTROL, DATA };
  enum class Transport { UNDEFINED = 0, PIPE, NETWORK, INPROC };

  struct Options {
    Options() = default;
    Options(const Options& other);
    Options& operator=(const Options& other);
    Type type = Type::SERVER;
    Role role = Role::DATA;
    Transport transport = Transport::PIPE;
  };

  using Ptr = std::shared_ptr<Endpoint>;
  using UPtr = std::unique_ptr<Endpoint>;
  using Error = boost::system::error_code;
  using ReadHandler = std::function<void(const Error&, azmq::message&, size_t)>;
  using StartStop = std::pair<std::function<void()>, std::function<void()>>;
  using DataSlot = std::function<void(VideoFrame::Ptr)>;

  Endpoint(BoostAsio* io, int socket_type);
  ~Endpoint() override;

  void FromProto(const proto::Endpoint* proto) override;
  void ToProto(proto::Endpoint* proto) const override;

  void Start();
  void Stop();
  bool IsStopped() const;
  void Subscribe(const std::string& = "");
  void Publish(VideoFrame::Ptr);
  void SetSndTimeo(int timeout = 0) { socket_.set_option(azmq::socket::snd_timeo(timeout)); }

  std::string GetAddress() const;
  std::string MakeAddress(const std::string&) const;
  std::string MakeAddress(uint16_t) const;

  Endpoint::Options GetOptions() const { return opts_; }

  void SetQueue(Queue::Ptr queue) { queue_ = queue; }

  template <typename T>
  void Init(const Options& opts, const T& param) {
    try {
      opts_ = opts;
      address_ = MakeAddress(param);

      const auto& start_method = start_stop_methods_.at(opts_.type).first;
      start_method();
    } catch (std::exception& e) {
      throw std::runtime_error(fmt::format("{}: {}", e.what(), address_));
    }
  }

  void RegisterServiceAsData() {
    read_handler_ = [&](const Error& ec, azmq::message& msg, size_t bytes) {
      if (ec) {
        LOG(ERROR) << ec.message() << address_;
        return;
      }

      if (IsStopped())
        return;

      auto frame = std::make_shared<VideoFrame>();
      gproto::ArenaOptions arena_opts;
      arena_opts.initial_block_size = bytes;

      gproto::Arena arena(arena_opts);
      proto::VideoFrame* frame_proto = gproto::Arena::CreateMessage<proto::VideoFrame>(&arena);

      frame_proto->ParseFromArray(msg.data(), static_cast<int>(msg.size()));

      frame->FromProto(frame_proto);
      queue_->Add(frame);

      StartRead();
    };
  }

  template <typename S>
  void RegisterServiceAsRPC(const S& service) {
    read_handler_ = [&](const Error& ec, azmq::message& msg, size_t bytes) {
      if (ec) {
        LOG(ERROR) << ec.message();
        return;
      }

      if (IsStopped())
        return;

      azmq::message_vector msgs{msg};

      if (msg.more())
        bytes += socket_.receive(msg);
      else
        throw std::logic_error(fmt::format("Empty ZeroMQ message from {}", msg.string()));

      gproto::Arena arena;
      proto::Packet* pkt = gproto::Arena::CreateMessage<proto::Packet>(&arena);
      pkt->ParseFromString(msg.string());

      const auto& method_name = pkt->method();
      const auto* method = service->GetDescriptor()->FindMethodByName(method_name);

      gproto::Message* request = service->GetRequestPrototype(method).New(&arena);
      gproto::Message* response = service->GetResponsePrototype(method).New(&arena);

      pkt->body().UnpackTo(request);
      service->CallMethod(method, nullptr, request, response, nullptr);
      pkt->mutable_body()->PackFrom(*response);

      size_t pkt_size = pkt->ByteSizeLong();
      azmq::message reply(pkt_size);

      pkt->SerializeToArray(boost::asio::buffer_cast<uchar*>(reply.buffer()), static_cast<int>(pkt_size));

      msgs.push_back(reply);
      socket_.send(msgs);
      StartRead();
    };
  }

 private:
  Options opts_;

  azmq::socket socket_;
  std::string address_;
  std::map<Transport, std::string> transport_schemes_;
  std::map<Type, StartStop> start_stop_methods_;

  Queue::Ptr queue_;
  ReadHandler read_handler_;

  std::promise<void> stop_signal_;
  std::future<void> stop_event_;

  void Bind();
  void Connect();
  void Unbind();
  void Disconnect();
  void StartRead();
  void ResetStopEvent();
  void SetStopEvent();
};

}  // namespace omv

#endif  // OMV_ENDPOINT_H
