#ifndef OMV_EVENT_H
#define OMV_EVENT_H

#include <boost/asio.hpp>

namespace asio = boost::asio;

namespace omv {

class Event {
 public:
#ifdef MODERN_BOOST
  using BoostAsio = boost::asio::io_context;
#else
  using BoostAsio = boost::asio::io_service;
#endif

  explicit Event(BoostAsio&);

  template <typename WaitHandler>
  void AsyncWait(WaitHandler handler) {
    timer_.async_wait(std::bind(handler));
  }

  void NotifyOne();
  void NotifyAll();

 private:
  asio::deadline_timer timer_;
};

}  // namespace omv

#endif  // OMV_EVENT_H
