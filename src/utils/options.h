#ifndef OMV_OPTIONS_H
#define OMV_OPTIONS_H

#include <map>
#include <string>
#include <variant>

#include "CLI/CLI.hpp"

#include "omv_version.h"

namespace omv {

class Options {
 public:
  using Variant = std::variant<int, double, std::string>;

  static Options& Instance() {
    static Options s;
    return s;
  }

  Options(Options const&) = delete;
  Options& operator=(Options const&) = delete;

  void Init(int argc, char** argv);

  class ProxyValue {
   public:
    ProxyValue(Options* opts, const std::string& k) : opts_{opts}, k_{k} {}

    template <typename T>
    operator T() {
      return opts_->Get<T>(k_);
    }

   private:
    Options* opts_;
    std::string k_;
  };

  ProxyValue operator[](const std::string& key) { return ProxyValue(this, key); }

  template <typename T>
  inline T Get(const std::string& key) const {
    return std::get<T>(data_.at(key));
  }

 private:
  Options() = default;
  CLI::App opts_{PROJECT_NAME};
  std::map<std::string, Variant> data_{
      {"demo-src", std::string()}, {"demo-dst", std::string()},         {"threads", 4},
      {"plugins", "plugins"},      {"data", "/usr/share/omvnn/models"}, {"fifo", "/tmp/feed"},
      {"bind-adr", "0.0.0.0"}};
};

}  // namespace omv

#endif  // OMV_OPTIONS_H
