#include "options.h"

#include "utils/logger.h"

namespace omv {

void Options::Init(int argc, char** argv) {
  opts_.add_option<Variant, std::string>("--demo-src", data_["demo-src"], "Run in Demo mode, RTSP source");
  opts_.add_option<Variant, std::string>("--demo-dst", data_["demo-dst"], "Run in Demo mode, RTP destination");
  opts_.add_option<Variant, int>("-t,--threads", data_["threads"], "Number of IO threads");
  opts_.add_option<Variant, std::string>("-p,--plugins", data_["plugins"], "Plugins directory");
  opts_.add_option<Variant, std::string>("-d,--data", data_["data"], "Data directory");
  opts_.add_option<Variant, std::string>("-f,--fifo", data_["fifo"], "FIFO pipes directory");
  opts_.add_option<Variant, std::string>("-b,--bind-adr", data_["bind-adr"], "IP address to bind");

  opts_.add_flag_function(
      "-v",
      [&](int count) {
        SeverityLevel log_level = SeverityLevel::DEBUG;

        if (count > 0 && count < 3)
          log_level = static_cast<SeverityLevel>(2 - count);

        SET_LOG_LEVEL(log_level);
      },
      "Verbose level");

  try {
    opts_.parse(argc, argv);
  } catch (const CLI::ParseError& e) {
    opts_.exit(e);
    throw e;
  }
}

}  // namespace omv
