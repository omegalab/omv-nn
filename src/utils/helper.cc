#include "helper.h"

#include <pthread.h>

#include <chrono>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <opencv2/opencv.hpp>

#include "utils/logger.h"

namespace omv {

Exception::Exception(const std::string& msg) : std::runtime_error{msg} {}

void Helper::SetTimeStamp(google::protobuf::Timestamp* msg) {
  int64_t ts = Helper::GetTimeStamp();

  Helper::SetTimeStamp(msg, ts);
}

void Helper::SetTimeStamp(google::protobuf::Timestamp* msg, int64_t ts) {
  int64_t seconds = ts / 1'000;
  int32_t millis = ts % 1'000;

  msg->set_seconds(seconds);
  msg->set_nanos(millis * 1'000'000);
}

void Helper::SetThreadName(const std::string& name) { pthread_setname_np(pthread_self(), name.c_str()); }

void Helper::SetThreadName(unsigned long thread_id, const std::string& name) {
  pthread_setname_np(thread_id, name.c_str());
}

int64_t Helper::GetTimeStamp() {
  using namespace std::chrono;
  return static_cast<int64_t>(duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count());
}

std::string Helper::GenerateUuid4() {
  using namespace boost::uuids;
  random_generator gen;
  uuid id = gen();
  return to_string(id);
}

std::vector<std::string> Helper::SplitString(const std::string& source, char delimiter) {
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream token_stream(source);

  while (std::getline(token_stream, token, delimiter)) {
    tokens.push_back(token);
  }
  return tokens;
}

int32_t FourCC::ToInt(const std::string& v) { return (((((v[3] << 8) | v[2]) << 8) | v[1]) << 8) | v[0]; }

std::string FourCC::FromInt(int32_t val) {
  std::string st(4, 0);
  st[0] = static_cast<char>(val & 0xff);
  st[1] = static_cast<char>((val >> 8) & 0xff);
  st[2] = static_cast<char>((val >> 16) & 0xff);
  st[3] = static_cast<char>((val >> 24) & 0xff);
  return st;
}

}  // namespace omv
