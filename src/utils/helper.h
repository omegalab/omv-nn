#ifndef OM_HELPER_H
#define OM_HELPER_H

#include <sstream>
#include <string>

#include <google/protobuf/timestamp.pb.h>

namespace omv {

struct Exception : public std::runtime_error {
  Exception(const std::string& msg);
};

struct FourCC {
  static int32_t ToInt(const std::string& v);
  static std::string FromInt(int32_t val);
};

class Helper {
 public:
  Helper() = default;

  template <typename It>
  static std::string PrintRange(const It& begin, const It& end) {
    std::ostringstream os;
    for (auto it = begin; it != end; ++it) {
      os << *it;
    }

    return os.str();
  }

  static int64_t GetTimeStamp();
  static void SetTimeStamp(google::protobuf::Timestamp*);
  static void SetTimeStamp(google::protobuf::Timestamp*, int64_t);

  static void SetThreadName(const std::string& name);
  static void SetThreadName(unsigned long, const std::string&);
  static std::string GenerateUuid4();
  static std::vector<std::string> SplitString(const std::string& source, char delimiter);
};

}  // namespace omv

#endif  // OM_HELPER_H
