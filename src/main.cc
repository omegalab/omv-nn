#include <iostream>

#include "omvnn/omvnn.h"

int main(int argc, char* argv[]) {
  int exit_code = EXIT_SUCCESS;

  try {
    omv::OmvNN(argc, argv).Start();

  } catch (const CLI::ParseError&) {
    exit_code = EXIT_FAILURE;

  } catch (const std::exception& e) {
    exit_code = EXIT_FAILURE;
    LOG(ERROR) << e.what();
  }

  return exit_code;
}
