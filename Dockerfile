FROM registry.gitlab.com/omegalab/opencv:4.2.0-cuda10.0-devel-ubuntu18.04 AS build

WORKDIR /omv_build

COPY cmake ./cmake
COPY plugins ./plugins
COPY protocol-va ./protocol-va
COPY src ./src
COPY tests ./tests
COPY third_party ./third_party
COPY CMakeLists.txt .

RUN apt-get update && apt-get install cmake pkg-config curl -y

RUN curl -sL https://github.com/protocolbuffers/protobuf/releases/download/v3.6.1/protobuf-cpp-3.6.1.tar.gz | tar zx && \
    cd protobuf-3.6.1 && mkdir build && cd build && \
    cmake \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_POSITION_INDEPENDENT_CODE=ON \
    -D protobuf_BUILD_TESTS=OFF \
    -D protobuf_BUILD_EXAMPLES=OFF \
    -D protobuf_WITH_ZLIB=OFF ../cmake && \
    make -j$(nproc) && make install

RUN mkdir build && cd build && \
    cmake \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_C_COMPILER=gcc-9 \
    -D CMAKE_CXX_COMPILER=g++-9 \
    -D OMVNN_BUILD_PLUGINS=ON \
    -D OMVNN_BUILD_TESTS=OFF \
    -D OMVNN_BUILD_PACKAGE=OFF \
    -D CMAKE_INSTALL_PREFIX=/usr/local/omvnn .. && \
    make -j$(nproc) && make install

FROM registry.gitlab.com/omegalab/opencv:4.2.0-cuda10.0-runtime-ubuntu18.04

COPY --from=build /usr/local/omvnn /usr/local/omvnn
COPY omvnnd.sh /usr/local/omvnn/bin/omvnnd.sh

ENV TZ=Europe/Moscow \
    OMVNN_PLUGINS_PATH="/usr/local/omvnn/plugins" \
    OMVNN_FLAGS="-v"

RUN groupadd -g 1000 omv && useradd -m -u 1000 -g omv omv

USER omv

CMD ["/usr/local/omvnn/bin/omvnnd.sh"]

