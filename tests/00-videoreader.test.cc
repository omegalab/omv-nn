#include "catch2/catch.hpp"

#include <chrono>
#include <opencv2/opencv.hpp>

#include "dvr/video_reader.h"
#include "dvr/video_reader_cuda.h"
#include "dvr/video_reader_ffmpeg.h"

namespace omg {

class VideoReaderTester {
 public:
  VideoReaderTester(VideoReader& vr) : vr_{vr} {}

  void SetPath(const std::string& path) {
    vr_.path_ = path;
  }

 private:
  VideoReader& vr_;
};

TEST_CASE("VideoReader Dummy Test", "[video_reader]") {
  omg::Logger::Instance().Init(10);

  SECTION("check FFmpeg reader") {
    FFmpegVideoReader vr;
    VideoReaderTester vr_tester(vr);

    vr_tester.SetPath("rtsp://192.168.161.91:554/mpeg4/media.amp");
    vr.Open();

    //    REQUIRE(vr.GetWidth() == 1920);
    //    REQUIRE(vr.GetHeight() == 1080);

    REQUIRE(vr.GetWidth() == 640);
    REQUIRE(vr.GetHeight() == 480);

    vr.Start();
    cv::namedWindow("frame", cv::WINDOW_AUTOSIZE);

    std::thread read_thread([&] {
      while (!vr.IsStopped()) {
        auto frame = vr.Read();

        REQUIRE(frame->GetM().empty() == false);

        cv::imshow("frame", frame->GetM());

        if (int k = cv::waitKey(33); k == 27)
          break;
      }

      cv::destroyAllWindows();
    });

    std::this_thread::sleep_for(std::chrono::seconds(5));

    vr.Close();
    read_thread.join();
  }

  /*
    SECTION("check Cuda reader") {
      CudaVideoReader vr;
      VideoReaderTester vr_tester(vr);

      vr_tester.SetPath("bbb_sunflower_1080p_30fps_normal.mp4");
      vr.Open();

      REQUIRE(vr.GetWidth() == 1920);
      REQUIRE(vr.GetHeight() == 1088);

      vr.Start();
      cv::namedWindow("frame", cv::WINDOW_AUTOSIZE);

      std::thread read_thread([&] {
        while (!vr.IsStopped()) {
          auto frame = vr.Read();

          REQUIRE(frame->GetM().empty() == false);

          cv::imshow("frame", frame->GetM());

          if (int k = cv::waitKey(33); k == 27)
            break;
        }

        cv::destroyAllWindows();
      });

      std::this_thread::sleep_for(std::chrono::seconds(5));

      vr.Close();
      read_thread.join();
    }
    */
}

}  // namespace omg
