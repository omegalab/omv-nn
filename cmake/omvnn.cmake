file(GLOB PROTOBUF_DEFINITION_FILES "${CMAKE_CURRENT_SOURCE_DIR}/protocol-va/omv/proto/*.proto")
set(PROTOBUF_IMPORT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/protocol-va")
set(PROTOBUF_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
foreach(file ${PROTOBUF_DEFINITION_FILES})
    execute_process(
      COMMAND protoc -I${PROTOBUF_IMPORT_DIRECTORY} --cpp_out=${PROTOBUF_OUTPUT_DIRECTORY} ${file}
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      RESULTS_VARIABLE PROTOBUF_RESULTS
      OUTPUT_VARIABLE PROTOBUF_OUTPUT_VARIABLE
      ERROR_VARIABLE PROTOBUF_ERROR_VARIABLE)
endforeach()

set(OMVPROTO_SRC
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/service.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/camera.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/common.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/dummy.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/endpoint.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/licplates_detector.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/matrix.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/packet.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/ping.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/pipeline.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/point.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/plugin.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/plugin_output.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/queue.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/register.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/rect.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/super_resolution.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/video_backend.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/dnn_backend.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/video_frame.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/video_reader.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/video_writer.pb.cc
  ${CMAKE_CURRENT_BINARY_DIR}/omv/proto/yolo_detector.pb.cc
)

set(OMVNN_SRC
  ${OMVNN_SOURCE_DIR}/omvnn/omvnn.cc
  ${OMVNN_SOURCE_DIR}/omvnn/base_plugin.cc
  ${OMVNN_SOURCE_DIR}/omvnn/base_service.cc
  ${OMVNN_SOURCE_DIR}/omvnn/channel.cc
  ${OMVNN_SOURCE_DIR}/omvnn/color.cc
  ${OMVNN_SOURCE_DIR}/omvnn/endpoint.cc
  ${OMVNN_SOURCE_DIR}/omvnn/event.cc
  ${OMVNN_SOURCE_DIR}/omvnn/master_service.cc
  ${OMVNN_SOURCE_DIR}/omvnn/registry.cc
  ${OMVNN_SOURCE_DIR}/omvnn/queue.cc
  ${OMVNN_SOURCE_DIR}/omvnn/video_frame.cc
  ${OMVNN_SOURCE_DIR}/utils/logger.cc
  ${OMVNN_SOURCE_DIR}/utils/helper.cc
  ${OMVNN_SOURCE_DIR}/utils/options.cc
)

add_library(omvnn SHARED ${OMVNN_SRC} ${OMVPROTO_SRC})
target_link_libraries(omvnn PUBLIC dl
  CLI11::CLI11
  fmt::fmt-header-only
  protobuf::libprotobuf
  Threads::Threads
  ${ZeroMQ_LIBRARIES}
  ${Boost_LIBRARIES}
  ${OpenCV_LIBS})

target_include_directories(omvnn PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}/third_party/azmq
  ${CMAKE_CURRENT_BINARY_DIR}
  ${OMVNN_SOURCE_DIR}
  ${OpenCV_INCLUDE_DIRS})

install (TARGETS omvnn LIBRARY DESTINATION lib)
if(OMV_BUILD_WITH_COTIRE)
  cotire(omvnn)
endif()
